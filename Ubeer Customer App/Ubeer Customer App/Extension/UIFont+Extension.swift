//
//  UIFont+Extension.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import Foundation
import UIKit

enum FontSize: CGFloat {
    case tabbarSize = 11
    case verySmall = 13
    case extraSmall = 14
    case small = 15
    case regular = 18
    case large = 20
    case extraLarge = 24
    case huge = 26
    case extraHuge = 80
}

struct AppFont {
    static func getFont(style: FontStyle, size: FontSize = .regular) -> UIFont {
        let fontSize: CGFloat = (CGFloat(screenWidth) * size.rawValue) / 375
        return UIFont(name: style.rawValue, size: fontSize)!
    }
}
