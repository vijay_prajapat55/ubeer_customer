//
//  APIService.swift
//  DietPro
//

import Foundation
import SwiftyJSON
import UIKit

class APIService {
    
    //---Post API Calling Method
    class func postApiCallingMethod(param: [String: Any], url: String,
                                    vc: UIViewController,
                                    showProgress: Bool = false,
                                    success: @escaping(_ response : JSON, _ isSuccess : Bool, _ code: Int) -> Void,
                                    failure: @escaping(_ error : Error) -> Void,
                                    noInternet: @escaping() -> Void) {
        
        ApiRequestHelper.requestWebService(url: url, methodType: .post, requireAuthorisation: false, parameters: param, showProgress: showProgress, vc: vc) { response, isSuccess, code in
            success(response, isSuccess , code)
        } failure: { error in
            failure(error)
        }
    }
    
    //---Post API Calling Method With Header
    class func postApiCallingWithHeaderMethod(param: [String: Any],
                                              url: String,
                                              vc: UIViewController,
                                              showProgress: Bool = false,
                                              success: @escaping(_ response : JSON, _ isSuccess : Bool, _ code: Int) -> Void,
                                              failure: @escaping(_ error : Error) -> Void,
                                              noInternet: @escaping() -> Void){
        
        ApiRequestHelper.requestWebService(url: url, methodType: .post, requireAuthorisation: true, parameters: param, showProgress: showProgress, vc: vc) { response, isSuccess, code in
            success(response, isSuccess , code)
        } failure: { error in
            failure(error)
        }
    }
    
    //---Post API Calling Method With Header And Image
    class func postApiCallingWithHeaderAndImageMethod(images: [UIImage],
                                                      photoParamName : [String],
                                                      param: [String: Any],
                                                      url: String,
                                                      success: @escaping(_ response : JSON, _ isSuccess : Bool) -> Void,
                                                      failure: @escaping(_ error : Error) -> Void,
                                                      noInternet: @escaping() -> Void){
        
        ApiRequestHelper.requestWebServiceWithImage(images: images, photoParamName: photoParamName, url: url, parameters: param, showProgress: true) { response, status in
            success(response, status)
        } failure: { error in
            failure(error)
        } progressHandler: { progress in
            
        }
    }
    
    //--Get API Calling Method
   // class func getReference(url: String, vc: UIViewController, success: @escaping(_ response: JSON, _ isSuccess: Bool) -> Void, failure: @escaping(_ error : Error) -> Void) {
//        ApiRequestHelper.requestWebService(url: url, methodType: .get, requireAuthorisation: false, showProgress: false, vc: vc) { (response, status) in
//            success(response, status)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
//
//    //--Get API Calling Method
//    class func getReferenceWithHeader(url: String, vc: UIViewController, showProgress: Bool = false, success: @escaping(_ response: JSON, _ isSuccess: Bool) -> Void, failure: @escaping(_ error : Error) -> Void) {
//        ApiRequestHelper.requestWebService(url: url, methodType: .get, requireAuthorisation: true, showProgress: showProgress, vc: vc) { (response, status) in
//            success(response, status)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
}
