//
//  MyProfileModel.swift
//  Go Food
//
//  Created by Vijay Prajapat on 16/05/22.
//

import Foundation
import SwiftyJSON

// MARK: - Welcome
class MyProfileModel {
    var responseData: ResponseDataProfile?
    var statuscode: Int?
    var message: String?
    var success: Bool?

    init(fromDictionary dictionary: JSON) {
        self.success = dictionary["success"].boolValue
        self.statuscode = dictionary["STATUSCODE"].intValue
        self.message = dictionary["message"].stringValue
        let response = dictionary["response_data"]
        self.responseData = ResponseDataProfile(fromDictionary: response)
    }
}

// MARK: - ResponseData
class ResponseDataProfile {
    var location, phone, firstName, lat: String?
    var lastName, profileImage, countryCode, lng: String?
    var email, dob: String?

    init(fromDictionary dictionary: JSON) {
        self.location = dictionary["location"].stringValue
        self.phone = dictionary["phone"].stringValue
        self.firstName = dictionary["firstName"].stringValue
        self.lat = dictionary["lat"].stringValue
        self.lastName = dictionary["lastName"].stringValue
        self.profileImage = dictionary["profileImage"].stringValue
        self.countryCode = dictionary["countryCode"].stringValue
        self.lng = dictionary["lng"].stringValue
        self.email = dictionary["email"].stringValue
        self.dob = dictionary["dob"].stringValue
    }
}
