//
//  SearchNavigationController.swift
//  Customer
//
//  Created by Pradipta on 05/08/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class SearchNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isHidden = true
    }
    
}
