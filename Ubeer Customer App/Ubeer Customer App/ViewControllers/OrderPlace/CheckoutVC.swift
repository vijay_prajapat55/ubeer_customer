//
//  CheckoutVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 20/01/22.
//

import UIKit
import CoreData

class CheckoutVC: UIViewController {
    
    @IBOutlet weak var lblTitle: CustomNormalLabel!
    @IBOutlet weak var lblOrderDeliveredBy: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblEstimatedTime: UILabel!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var txtViewSpecialInstruction: UITextView!
    @IBOutlet weak var constraintHeightTextView: NSLayoutConstraint!
    @IBOutlet weak var itemsTableView: UITableView! {
        didSet {
            itemsTableView.register(UINib(nibName: "CartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "CartItemTableViewCell")
            itemsTableView.addObserver(self, forKeyPath: "contentSize", options: .new ,context: nil)
        }
    }
    @IBOutlet weak var constraintHeightTableView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightView: NSLayoutConstraint!
    @IBOutlet weak var lblAddATip: UILabel!
    @IBOutlet weak var btn10Percent: UIButton!
    @IBOutlet weak var btn20Percent: UIButton!
    @IBOutlet weak var btn30Percent: UIButton!
    @IBOutlet weak var otherPercent: UIButton!
    @IBOutlet weak var lblTipNote: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var txtFldPromoCode: UITextField!
    @IBOutlet weak var lblSubTotalTitle: UILabel!
    @IBOutlet weak var lblSubTotalAmount: UILabel!
    @IBOutlet weak var lblPromoTitle: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var lblPromoCodeAmount: UILabel!
    @IBOutlet weak var lblTipTitle: UILabel!
    @IBOutlet weak var lblTipAmount: UILabel!
    @IBOutlet weak var lblDeliveryChargesTitle: UILabel!
    @IBOutlet weak var lblDeliveryChargesAmount: UILabel!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPayBy: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var btnSelectPayment: UIButton!
    @IBOutlet weak var btnCheckMarkContactlessDelivery: UIButton!
    @IBOutlet weak var lblContanctlessDelivery: UILabel!
    @IBOutlet weak var lblPlaceOrder: UILabel!
    @IBOutlet weak var lblPlaceOrderAmount: UILabel!
    
    var itemsArray: [[String: Any]]? = []
    var itemCount = 0
    var model: CheckoutModel!
    var subtotal = 0.0
    var tipAmount = 0.0
    var totalAmount = 0
    var items = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataFetch()
        self.lblAddress.text = userAddress
        self.lblCustomerName.text = customerName
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if object is UITableView {
                if let newValue  = change?[.newKey] {
                    let newsize = newValue as! CGSize
                    self.constraintHeightTableView.constant = newsize.height
                    self.constraintHeightView.constant = 1010 + newsize.height
                }
            }
        }
    }
    
    static func getInstance() -> CheckoutVC {
        return R.storyboard.orderPlace.checkoutVC()!
    }
    
    func setupAmounts() {
        //  self.lblSubTotalAmount.text = "$\((self.subtotal * 100).rounded() / 100)"
        self.subtotal = 0.0
        
        for item in itemsArray! {
            self.subtotal += (item["price"] as? Double ?? 0.0) * Double(item["quantity"] as! Int)
        }
        let roundedSubTotalAmount = "$\((self.subtotal * 100).rounded() / 100)"
        self.lblSubTotalAmount.text = roundedSubTotalAmount
        self.lblPromoCodeAmount.text = "$\((0.00 * 100).rounded() / 100)"
        self.lblTipAmount.text = "$\((self.tipAmount * 100).rounded() / 100)"
        self.lblDeliveryChargesAmount.text = "$\(model.responseData?.deliveryCost ?? "")"
        let totalAmount = (self.subtotal) + (self.tipAmount) + (Double(model.responseData?.foodServiceCost ?? "") ?? 0.0) + (Double(model.responseData?.deliveryCost ?? "") ?? 0.0)
        let roundedTotalAmount = "$\((totalAmount * 100).rounded() / 100)"
        self.lblTotalAmount.text = roundedTotalAmount
        self.lblPlaceOrderAmount.text = roundedTotalAmount
        self.totalAmount = Int(((totalAmount * 100).rounded()))
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeAddressAction(_ sender: Any) {
        self.navigationController?.pushViewController(SelectLocationVC.getInstance(), animated: true)
    }
    
    @IBAction func btn10PercentAction(_ sender: Any) {
        self.tipAmount = 0.0
        self.tipAmount = self.subtotal * 0.10
        self.lblTipAmount.text = "$\((self.tipAmount * 100).rounded() / 100)"
        checkFarePICalling()
    }
    
    @IBAction func btn20PercentAction(_ sender: Any) {
        self.tipAmount = 0.0
        self.tipAmount = self.subtotal * 0.20
        self.lblTipAmount.text = "$\((self.tipAmount * 100).rounded() / 100)"
        checkFarePICalling()
    }
    
    @IBAction func btn30PercentAction(_ sender: Any) {
        self.tipAmount = 0.0
        self.tipAmount = self.subtotal * 0.30
        self.lblTipAmount.text = "$\((self.tipAmount * 100).rounded() / 100)"
        checkFarePICalling()
    }
    
    @IBAction func btnOtherPercentAction(_ sender: Any) {
        let vc = EnterManuallyTipVC.getInstance()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnApplyCouponCodeAction(_ sender: Any) {
        
    }
    
    @IBAction func btnSelectPaymentAction(_ sender: Any) {
        
    }
    
    @IBAction func btnCheckMarkContactlessDeliveryAction(_ sender: UIButton) {
        self.btnCheckMarkContactlessDelivery.isSelected = !self.btnCheckMarkContactlessDelivery.isSelected
        sender.setImage(self.btnCheckMarkContactlessDelivery.isSelected ? R.image.squareRadioButtonSelected()! : R.image.squareRadioButton()!, for: .normal)
    }
    
    @IBAction func btnPlaceOrderAction(_ sender: Any) {
        let vc = SelectCardVC.getInstance()
        vc.totalAmount = self.totalAmount
        vc.price = subtotal
        vc.items = self.items
        vc.deliveryCost = "\(model.responseData?.deliveryCost ?? "")"
        vc.foodServiceCost = "0"
        vc.tipAmount = "\((self.tipAmount * 100).rounded() / 100)"
        vc.deliveryInstruction = self.txtViewSpecialInstruction.text ?? ""
        vc.vendorId = itemsArray?.first?["vendorId"] as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CheckoutVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemTableViewCell", for: indexPath) as? CartItemTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        if let model = itemsArray?[indexPath.row] {
            cell.lblItemName.text = model["name"] as? String
            cell.lblPrice.text = "\(model["price"] as? Double ?? 0.0)"
            //self.itemCount = model["quantity"] as! Int
            cell.lblQuantity.text = "\(model["quantity"] as! Int)"
            cell.itemCount = model["quantity"] as! Int
            cell.btnRemove.tag = indexPath.row
        }
        return cell
    }
}

extension CheckoutVC {
    func dataFetch() {
        self.subtotal = 0.0
        self.itemsArray?.removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CartItems")
        do {
            let myData = try managedContext.fetch(fetchRequest)
            for data in myData {
                let name = data.value(forKey: "name") as! String
                let itemId = data.value(forKey: "itemId") as! String
                let menuImage = data.value(forKey: "menuImage") as! String
                let categoryId = data.value(forKey: "categoryId") as! String
                let quantity = data.value(forKey: "quantity") as! Int
                let waitingTime = data.value(forKey: "waitingTime") as! String
                let vendorId = data.value(forKey: "vendorId") as! String
                let productId = data.value(forKey: "productId") as! String
                let totalPrice = data.value(forKey: "totalPrice") as! Double
                let price = data.value(forKey: "price") as! Double
                
                var dict: [String: Any] = ["name": name, "itemId": itemId, "menuImage": menuImage, "quantity": quantity, "categoryId": categoryId, "waitingTime": waitingTime, "vendorId": vendorId, "productId": productId, "totalPrice": totalPrice, "price": price] 
                self.itemsArray?.append(dict)
                var item: [String: Any] = dict.removeValue(forKey: "productId") as! [String : Any]
                var a: [String: Any] = item.removeValue(forKey: "vendorId") as! [String : Any]
                a.switchKey(fromKey: "totalPrice", toKey: "price")
                print(a)
                
            }
            for item in itemsArray! {
                self.subtotal += item["totalPrice"] as! Double
                self.items += item["name"] as! String
            }
            self.itemsTableView.reloadData()
            self.checkFarePICalling()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteData(index: Int) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // Create Fetch Request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItems")
        let result = try? managedContext.fetch(request)
        let resultData = result as! [NSManagedObject]
        
        managedContext.delete(resultData[index])
        
        do {
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
}

extension CheckoutVC: CartItemProtocol {
    func ItemsCount(count: Int, cell: UITableViewCell, isPlus: Bool) {
        // self.itemCount = count
        let index = itemsTableView.indexPath(for: cell)
        let cartCell = cell as! CartItemTableViewCell
        cartCell.lblQuantity.text = String(count)
        
        let item = itemsArray?[index!.row]
        if isPlus {
            self.subtotal += (item?["price"] as? Double ?? 0.0) * 1.0
        } else {
            self.subtotal -= (item?["price"] as? Double ?? 0.0) * 1.0
        }
        let roundedSubTotalAmount = "$\((self.subtotal * 100).rounded() / 100)"
        self.lblSubTotalAmount.text = roundedSubTotalAmount
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CartItems")
        do {
            let myData = try managedContext.fetch(fetchRequest)
            myData[index!.row].setValue(count, forKey: "quantity")
            
            do {
                try managedContext.save()
            } catch _ {
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func removeItem(cell: UITableViewCell) {
        let index = itemsTableView.indexPath(for: cell)!.row
        
        deleteData(index: index)
        self.itemsArray?.remove(at: index)
        self.itemsTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if itemsArray?.count == 0 {
            self.navigationController?.popViewController(animated: true)
        }
        self.itemsTableView.reloadData()
        self.dataFetch()
    }
}

//MARK: - API Calling
extension CheckoutVC {
    func checkFarePICalling() {
        let vendorId = itemsArray?.first?["vendorId"] as? String ?? ""
        
        let para: [String: Any] = ["customerId": "\(userId)", "vendorId": vendorId, "deliveryLat": "22.540063757177077", "deliveryLong": "88.36572373471573", "userType": "CUSTOMER", "foodCost": "\(self.subtotal)"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.checkFare, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                self.model = CheckoutModel(fromDictionary: response)
                self.setupAmounts()
                self.itemsTableView.reloadData()
                print("s")
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
    
    func applyPromoCodePICalling() {
        let vendorId = itemsArray?.first?["vendorId"] as? String ?? ""
        let para: [String: Any] = ["customerId": userId, "vendorId": vendorId, "subTotal": "", "deliveryCharge": "", "userType": "CUSTOMER", "allTotal": "", "promoCode": ""]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.promoCode, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                //                self.model = CheckoutModel(fromDictionary: response)
                //                self.setupAmounts()
                //                self.itemsTableView.reloadData()
                print("s")
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}

extension CheckoutVC: ManualTipProtocol {
    func getManualTip(tipAmount: String) {
        let tipPercent = Double(tipAmount)!
        self.tipAmount = 0.0
        self.tipAmount = self.subtotal * (tipPercent / 100)
        self.lblTipAmount.text = "$\(self.tipAmount)"
        checkFarePICalling()
    }
}

extension Dictionary {
    mutating func switchKey(fromKey: Key, toKey: Key) {
        if let entry = removeValue(forKey: fromKey) {
            self[toKey] = entry
        }
    }
}
