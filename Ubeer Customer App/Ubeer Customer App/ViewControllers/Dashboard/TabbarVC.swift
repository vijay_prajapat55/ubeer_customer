//
//  TabbarVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 07/01/22.
//

import UIKit

class TabbarVC: SOTabBarController {

     var firstVC: HomeNavigationController?
    private var secondVC: SearchNavigationController?
    private var thirdVC: CartNavigationController?
    private var fourthVC: MyProfileNavigationController?
    
    override func loadView() {
        super.loadView()
        SOTabBarSetting.tabBarBackground = .white
        SOTabBarSetting.tabBarAnimationDurationTime = 0.0
        SOTabBarSetting.tabBarTintColor = .black
        SOTabBarSetting.tabBarTitleFont = AppFont.getFont(style: .normal, size: .small)
        SOTabBarSetting.tabBarTitleSelectedColor = .black
        SOTabBarSetting.tabBarTitleDeSelectedColor = .darkGray
    }
    
    deinit {
        //NotificationCenter.default.removeObserver(self, name: Notification.Name("SideMenuTapped"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(openSideMenu), name: Notification.Name("SideMenuTapped"), object: nil)
        
        firstVC = HomeNavigationController.init(rootViewController: DashboardVC.getInstance())
        secondVC = SearchNavigationController.init(rootViewController: SearchVC.getInstance())
        thirdVC = CartNavigationController.init(rootViewController: CartVC.getInstance())
        fourthVC = MyProfileNavigationController.init(rootViewController: MyProfileVC.getInstance())
        
        firstVC?.tabBarItem = UITabBarItem(title: AppStrings.home, image: UIImage(named: "home-tabbar-deselected"), selectedImage: UIImage(named: "home-tabbar-selected"))
        secondVC?.tabBarItem = UITabBarItem(title: AppStrings.search, image: UIImage(named: "search-tabbar-deselected"), selectedImage: UIImage(named: "search-tabbar-selected"))
        thirdVC?.tabBarItem = UITabBarItem(title: AppStrings.orders, image: UIImage(named: "orders-tabbar-deselected"), selectedImage: UIImage(named: "orders-tabbar-selected"))
        fourthVC?.tabBarItem = UITabBarItem(title: AppStrings.account, image: UIImage(named: "account-tabbar-deselected"), selectedImage: UIImage(named: "account-tabbar-selected"))
        
//        if let homeScreen = firstVC?.viewControllers.first as? HomeScreen {
//            homeScreen.delegate = self
//        }
//        if let searchScreen = secondVC?.viewControllers.first as? SearchView {
//            searchScreen.delegate = self
//        }
//        if let myProfileScreen = fourthVC?.viewControllers.first as? MyProfileView {
//            myProfileScreen.delegate = self
//        }
        
        guard let firstVC = self.firstVC, let secondVC = self.secondVC, let thirdVC = self.thirdVC, let fourthVC = self.fourthVC else { return }
        viewControllers = [firstVC, secondVC, thirdVC, fourthVC]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //NotificationCenter.default.removeObserver(self, name: Notification.Name("SideMenuTapped"), object: nil)
    }
    
    static func getInstance() -> TabbarVC {
        return R.storyboard.dashboard.tabbarVC()!
    }
    
//    private func openSideMenu() {
//        LeftMenuViewController.showLeftMenu(onParentViewController: self) { (selectedObj, selectedPosition) in
//
//        }
//        LeftMenuViewController.setDelegate(self)
//    }
    
    override func tabBar(_ tabBar: SOTabBar, didSelectTabAt index: Int) {
        if index == 0 || index == 1 {
            super.tabBar(tabBar, didSelectTabAt: index)
        } else {
            //need to check is login condition here
//            if UserDefaultValues.customerLoginType == AppConstants.guestUserType {
//                //working code
//                self.navigateToLogin()
//                self.tabBar.didSelectTab(index: 0)
//            } else {
                super.tabBar(tabBar, didSelectTabAt: index)
          //  }
        }
    }
    
//    private func navigateToLogin() {
//        let nvc = DashboardNavigationController.instantiateFromAppStoryboard(appStoryboard: .dashboard)
//        nvc.modalTransitionStyle = .crossDissolve
//        nvc.modalPresentationStyle = .fullScreen
//        let vc = InitialLoginView.instantiateFromAppStoryboard(appStoryboard: .main)
//        nvc.viewControllers = [vc]
//        self.navigationController?.present(nvc, animated: true, completion: nil)
//    }
    
}

//extension DashBoardView: LeftMenuViewControllerDelegate {
//
//    func clickedMenuHeader() {
//
//    }
//
//    func clickedIndexPath(index: Int) {
//        Utility.log("Menu Index : \(index)")
//        switch index {
//        case 0:
//            let vc = FavouritesRestaurantsView.instantiateFromAppStoryboard(appStoryboard: .favourites)
//            self.navigationController?.pushViewController(vc, animated: true)
//        case 1:
//            let vc = NotificationSettingsView.instantiateFromAppStoryboard(appStoryboard: .notificationSettings)
//            self.navigationController?.pushViewController(vc, animated: true)
//        default:
//            break
//        }
//    }
//
//    func clickedLogout() {
//
//    }
//
//    func clickedJoinAsAMember() {
//        self.navigateToLogin()
//    }
//
//}
//
//extension DashBoardView: HomeScreenDelegate {
//
//    func menuTappedHomeScreen() {
//        self.openSideMenu()
//    }
//
//}
//
//extension DashBoardView: MyProfileViewDelegate {
//
//    func menuTappedMyProfileScreen() {
//        self.openSideMenu()
//    }
//
//}
//
//extension DashBoardView: SearchViewDelegate {
//
//    func menuTappedSearchScreen() {
//        self.openSideMenu()
//    }
//
//}
