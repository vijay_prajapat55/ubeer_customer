//
//  AddressTableViewCell.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 16/02/22.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnTickMark: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
