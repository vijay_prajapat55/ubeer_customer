//
//  OrderPlacedVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 24/01/22.
//

import UIKit

class OrderPlacedVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> OrderPlacedVC {
        return R.storyboard.orderPlace.orderPlacedVC()!
    }

}
