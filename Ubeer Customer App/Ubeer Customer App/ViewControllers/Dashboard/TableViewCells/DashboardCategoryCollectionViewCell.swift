//
//  DashboardCategoryCollectionViewCell.swift
//  Customer
//
//  Created by Pradipta on 09/08/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class DashboardCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBorderContainer: UIView!
    @IBOutlet weak var imageViewCategory: UIImageView!
    @IBOutlet weak var labelCategory: CustomNormalLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelCategory.font = AppFont.getFont(style: .normal, size: .verySmall)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewBorderContainer.setCircularCorner()
        self.imageViewCategory.setCircularCorner()
    }

}
