//
//  DashboardBannersContainerTableViewCell.swift
//  Customer
//
//  Created by Pradipta on 16/11/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class DashboardBannersContainerTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewBanner: UICollectionView!
    @IBOutlet weak var pageControlBanner: UIPageControl!
    
    var model: [BannerDatum]?
    var baseUrl = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionViewBanner.register(UINib(nibName: "BannersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "bannersCollectionViewCell")
        self.collectionViewBanner.dataSource = self
        self.collectionViewBanner.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reload(model: [BannerDatum]) {
        self.collectionViewBanner.reloadData()
        self.pageControlBanner.numberOfPages = self.model?.count ?? 0
        self.pageControlBanner.currentPage = Int(self.collectionViewBanner.contentOffset.x) / Int(self.collectionViewBanner.frame.width)
    }
}

extension DashboardBannersContainerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model?.count ?? 0
        //self.presenter?.arrBaners.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannersCollectionViewCell", for: indexPath) as? BannersCollectionViewCell else { return UICollectionViewCell() }
        let imageUrl = "\(self.baseUrl)\(self.model?[indexPath.row].image ?? "")"
        cell.image_Banner.sd_setImage(with: URL(string : imageUrl), placeholderImage: nil) { (img, err, type, url) in
        }
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        cell.labelRestaurantNames.text = "Rajasthan Dhaba"
        //self.presenter?.arrBaners[indexPath.item].vendor
        cell.labelRestaurantNames.numberOfLines = 2
        cell.labelRestaurantNames.sizeToFit()
        cell.labelOfferText.text = self.model?[indexPath.row].offerText
        //self.presenter?.arrBaners[indexPath.item].offerText
        cell.labelOfferText.sizeToFit()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 16.0, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let banner = self.presenter?.arrBaners[indexPath.item]
//        self.bannerTapped?(self, banner)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pageControlBanner.currentPage = Int(ceil(scrollView.contentOffset.x / scrollView.frame.width))
    }

}
