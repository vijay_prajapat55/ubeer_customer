//
//  MyProfileNavigationController.swift
//  Customer
//
//  Created by Pradipta on 17/06/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class MyProfileNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isHidden = true
    }

}
