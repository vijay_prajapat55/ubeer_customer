//
//  DashboardModel.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 11/01/22.

import Foundation
import SwiftyJSON

// MARK: - DashboardModel
class DashboardModel {
    
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: ResponseData?
    
    init(fromDictionary dictionary: JSON) {
        self.success = dictionary["success"].boolValue
        self.statuscode = dictionary["STATUSCODE"].intValue
        self.message = dictionary["message"].stringValue
        let response = dictionary["response_data"]
        self.responseData = ResponseData(fromDictionary: response)
    }
}

// MARK: - ResponseData
class ResponseData {
    var vendor: [Vendor]
    var categoryData: [CategoryDatum]
    var categoryImageURL: String?
    var bannerData: [BannerDatum]
    var bannerImageURL: String?
    var badgeCount: Int?
    
    init(fromDictionary dictionary: JSON) {
        self.vendor = [Vendor]()
        let vendorArray = dictionary["vendor"].arrayValue
        
        for dic in vendorArray {
            let value = Vendor(fromDictionary: dic)
            self.vendor.append(value)
        }
        
        self.categoryData = [CategoryDatum]()
        let categoryArray = dictionary["category_data"].arrayValue
        
        for dic in categoryArray {
            let value = CategoryDatum(fromDictionary: dic)
            self.categoryData.append(value)
        }
        self.categoryImageURL = dictionary["category_imageUrl"].stringValue
        
        self.bannerData = [BannerDatum]()
        let bannerArray = dictionary["banner_data"].arrayValue
        
        for dic in bannerArray {
            let value = BannerDatum(fromDictionary: dic)
            self.bannerData.append(value)
        }
        self.bannerImageURL = dictionary["banner_imageUrl"].stringValue
        self.badgeCount = dictionary["badgeCount"].intValue
    }
}

// MARK: - BannerDatum
class BannerDatum {
    var bannerType, image, offerText, id: String?
    var vendorID, vendor: String?
    var type: Int?
    var delivery: Bool?
    
    init(fromDictionary dictionary: JSON) {
        self.bannerType = dictionary["bannerType"].stringValue
        self.image = dictionary["image"].stringValue
        self.offerText = dictionary["offerText"].stringValue
        self.id = dictionary["_id"].stringValue
        self.vendorID = dictionary["vendorId"].stringValue
        self.vendor = dictionary["vendor"].stringValue
        self.type = dictionary["type"].intValue
        self.delivery = dictionary["delivery"].boolValue
    }
}

// MARK: - CategoryDatum
class CategoryDatum {
    var id, categoryName, image: String?
    
    init(fromDictionary dictionary: JSON) {
        self.id = dictionary["_id"].stringValue
        self.categoryName = dictionary["categoryName"].stringValue
        self.image = dictionary["image"].stringValue
    }
}

// MARK: - Vendor
class Vendor {
    var id, name, logo: String?
    var banner: String?
    var rating: Int?
    var delivery: Bool?
    var address: String?
    var category: [Category]
    var distance: String?
    var deliveryCharge, latitude, longitude: Double?
    var favorite: Int?
    var offer: String?
    var restaurantClose: Bool?
    
    init(fromDictionary dictionary: JSON) {
        self.id = dictionary["id"].stringValue
        self.name = dictionary["name"].stringValue
        self.logo = dictionary["logo"].stringValue
        self.banner = dictionary["banner"].stringValue
        self.rating = dictionary["rating"].intValue
        self.delivery = dictionary["delivery"].boolValue
        self.address = dictionary["address"].stringValue
        
        self.category = [Category]()
        let categoryArray = dictionary["category"].arrayValue
        
        for dic in categoryArray {
            let value = Category(fromDictionary: dic)
            self.category.append(value)
        }
        
        self.distance = dictionary["distance"].stringValue
        self.deliveryCharge = dictionary["deliveryCharge"].doubleValue
        self.latitude = dictionary["latitude"].doubleValue
        self.longitude = dictionary["longitude"].doubleValue
        self.favorite = dictionary["favorite"].intValue
        self.offer = dictionary["offer"].stringValue
        self.restaurantClose = dictionary["restaurantClose"].boolValue
    }
}

// MARK: - Category
class Category {
    var isActive, isDeleted: Bool?
    var sortOrder: Int?
    var id, categoryName, image, createdAt: String?
    var updatedAt: String?
    var v: Int?
    
    init(fromDictionary dictionary: JSON) {
        self.isActive = dictionary["isActive"].boolValue
        self.isDeleted = dictionary["isDeleted"].boolValue
        self.sortOrder = dictionary["sortOrder"].intValue
        self.id = dictionary["_id"].stringValue
        self.categoryName = dictionary["categoryName"].stringValue
        self.image = dictionary["image"].stringValue
        self.createdAt = dictionary["createdAt"].stringValue
        self.updatedAt = dictionary["updatedAt"].stringValue
        self.v = dictionary["__v"].intValue
    }
}
