//
//  ForgotPasswordVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 04/01/22.
//

import UIKit
import SwiftyJSON

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppFont.getFont(style: .normal, size: .large)
        }
    }
    @IBOutlet weak var lblEnterMobileNumber: UILabel! {
        didSet {
            lblEnterMobileNumber.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    @IBOutlet weak var lblYouWillGetOTP: UILabel! {
        didSet {
            lblYouWillGetOTP.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var txtFldCountryCode: UITextField! {
        didSet {
            txtFldCountryCode.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var txtFldMobileNumber: UITextField! {
        didSet {
            txtFldMobileNumber.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var btnSendVerificationCode: UIButton! {
        didSet {
            btnSendVerificationCode.layer.cornerRadius = UIScreen.main.bounds.height / 36
            btnSendVerificationCode.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> ForgotPasswordVC {
        return R.storyboard.login.forgotPasswordVC()!
    }
    
    func isValidate() -> Bool {
        if txtFldMobileNumber.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterMobileNumber, vc: self)
            return false
        }
        return true
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryCodeDropDownAction(_ sender: Any) {
        let vc = CountryPickerViewController.getInstance()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSendVerificationCodeAction(_ sender: Any) {
        forgotPasswordAPICall()
    }
}

extension ForgotPasswordVC: CountryPickerViewControllerDelegate {
    func didSelectCountry(country: Country) {
        self.imgViewFlag.image = UIImage(named: country.flag!)
        self.txtFldCountryCode.text = country.dialCode!
    }
}

//MARK: - API Calling
extension ForgotPasswordVC {
    func forgotPasswordAPICall() {
        if isValidate() {
            let para: [String: Any] = ["phone": txtFldMobileNumber.text!, "userType": "customer"]
            
            APIService.postApiCallingWithHeaderMethod(param: para, url: API.forgotPassword, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    let data = response["response_data"]
                    let otp = data["forgotPassOtp"].stringValue
                    print(response)
                    let vc = VerifyOTPVC.getInstance()
                    vc.isComeFromForgotPassword = true
                    vc.otp = otp
                    vc.phoneNumber = self.txtFldMobileNumber.text!
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    let message = response["message"].stringValue
                    ErrorHanding().showAlert(message: message, vc: self)
                }
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
}

