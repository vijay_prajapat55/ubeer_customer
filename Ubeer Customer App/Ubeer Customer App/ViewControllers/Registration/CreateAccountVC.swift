//
//  CreateAccountVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 30/12/21.
//

import UIKit
import CoreLocation
import GooglePlaces
import SwiftyJSON
import Toast_Swift

class CreateAccountVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var lblFirstNameTitle: UILabel! {
        didSet {
            lblFirstNameTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldFirstName: UITextField! {
        didSet {
            txtFldFirstName.delegate = self
            txtFldFirstName.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblLastNameTitle: UILabel! {
        didSet {
            lblLastNameTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldLastName: UITextField! {
        didSet {
            txtFldLastName.delegate = self
            txtFldLastName.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblEmailAddressTitle: UILabel! {
        didSet {
            lblEmailAddressTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldEmailAddress: UITextField! {
        didSet {
            txtFldEmailAddress.delegate = self
            txtFldEmailAddress.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblLocationTitle: UILabel! {
        didSet {
            lblLocationTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldLocation: UITextField! {
        didSet {
            txtFldLocation.delegate = self
            txtFldLocation.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblDOBTitle: UILabel! {
        didSet {
            lblDOBTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtfldDOB: UITextField! {
        didSet {
            txtfldDOB.delegate = self
            txtfldDOB.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblMobileNoTitle: UILabel! {
        didSet {
            lblMobileNoTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldMobileNumber: UITextField! {
        didSet {
            txtFldMobileNumber.delegate = self
            txtFldMobileNumber.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldCountryCode: UITextField! {
        didSet {
            txtFldCountryCode.delegate = self
            txtFldCountryCode.font = AppFont.getFont(style: .normal, size: .extraSmall)
            self.txtFldCountryCode.text = "+91"
        }
    }
    @IBOutlet weak var imgViewCountryFlag: UIImageView! {
        didSet {
            self.imgViewCountryFlag.image = UIImage(named: "in")
        }
    }
    @IBOutlet weak var lblPasswordTitle: UILabel! {
        didSet {
            lblPasswordTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldPassword: UITextField! {
        didSet {
            txtFldPassword.delegate = self
            txtFldPassword.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblConfirmPasswordTitle: UILabel! {
        didSet {
            lblConfirmPasswordTitle.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldConfirmPassword: UITextField! {
        didSet {
            txtFldConfirmPassword.delegate = self
            txtFldConfirmPassword.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var lblPasswordNote: UILabel! {
        didSet {
            lblPasswordNote.font = AppFont.getFont(style: .normal, size: .tabbarSize)
        }
    }
    @IBOutlet weak var lblTermsAndConditionMsg: UILabel! {
        didSet {
            lblTermsAndConditionMsg.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var lblAnd: UILabel! {
        didSet {
            lblAnd.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var btnTermsAndCondition: UIButton! {
        didSet {
            btnTermsAndCondition.titleLabel?.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var btnPrivacyPolicy: UIButton! {
        didSet {
            btnPrivacyPolicy.titleLabel?.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var btnSignUp: UIButton! {
        didSet {
            btnSignUp.titleLabel?.font = AppFont.getFont(style: .bold, size: .small)
            btnSignUp.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnSignIn: UIButton! {
        didSet {
            btnSignIn.titleLabel?.font = AppFont.getFont(style: .bold, size: .small)
        }
    }
    @IBOutlet weak var lblSignIn: UILabel! {
        didSet {
            lblSignIn.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    //MARK: - Variables
    var datePicker = UIDatePicker()
    
    //MARK: - App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create Account"
        addBackButtonToNavigation()
        if #available(iOS 13.4, *) {
            showDatePicker()
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Class Instance
    static func getInstance() -> CreateAccountVC {
        return R.storyboard.registration.createAccountVC()!
    }
    
    //MARK: - Other Methods
    func addBackButtonToNavigation() {
        let backBtn = UIBarButtonItem(image: R.image.back()!, style: .plain, target: self, action: #selector(btnBackAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    func isValidate() -> Bool {
        if txtFldFirstName.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterFirstName, vc: self)
            return false
        }
        
        if txtFldLastName.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterLastName, vc: self)
            return false
        }
        
        if txtFldEmailAddress.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterEmail, vc: self)
            return false
        }
        
        if txtFldLocation.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterLocation, vc: self)
            return false
        }
        
        if txtfldDOB.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterDOB, vc: self)
            return false
        }
        
        if txtFldMobileNumber.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterMobileNumber, vc: self)
            return false
        }
        
        if txtFldPassword.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterPassword, vc: self)
            return false
        }
        
        if txtFldConfirmPassword.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterConfirmPassword, vc: self)
            return false
        }
        return true
    }
    
    @objc func btnBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Button Actions
    @IBAction func btnCountryCodeDropDownAction(_ sender: UIButton) {
        let vc = CountryPickerViewController.getInstance()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnLocationAction(_ sender: UIButton) {
        autocompleteClicked()
    }
    
    @IBAction func btnDOBAction(_ sender: UIButton) {
        if #available(iOS 13.4, *) {
            showDatePicker()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func btnEyePassword(_ sender: UIButton) {
        self.txtFldPassword.isSecureTextEntry = !self.txtFldPassword.isSecureTextEntry
        sender.setImage(self.txtFldPassword.isSecureTextEntry ? UIImage(named: "eyeClosed") : UIImage(named: "eyeOpen"), for: .normal)
    }
    
    @IBAction func btnEyeConfirmPassword(_ sender: UIButton) {
        self.txtFldConfirmPassword.isSecureTextEntry = !self.txtFldConfirmPassword.isSecureTextEntry
        sender.setImage(self.txtFldConfirmPassword.isSecureTextEntry ? UIImage(named: "eyeClosed") : UIImage(named: "eyeOpen"), for: .normal)
    }
    
    @IBAction func btnTermsAndConditionsAction(_ sender: UIButton) {
        
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: UIButton) {
        
    }
    
    @IBAction func btnSignUpAction(_ sender: UIButton) {
        createAcccountAPICall()
    }
    
    @IBAction func btnSignInAction(_ sender: UIButton) {
        let vc = LoginVC.getInstance()
        vc.isComeFromSignUp = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - DatePicker Methods
@available(iOS 13.4, *)
extension CreateAccountVC {
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtfldDOB.inputAccessoryView = toolbar
        txtfldDOB.inputView = datePicker
    }
    
    @objc func donedatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd / MM / yyyy"
        txtfldDOB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
}

//MARK: - UITextField Delegate Methods
extension CreateAccountVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtFldCountryCode {
            let vc = CountryPickerViewController.getInstance()
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
}

//MARK: - CountryPicker Methods
extension CreateAccountVC: CountryPickerViewControllerDelegate {
    func didSelectCountry(country: Country) {
        self.imgViewCountryFlag.image = UIImage(named: country.flag!)
        self.txtFldCountryCode.text = country.dialCode!
    }
}

//MARK: - Google places autocorrect API
extension CreateAccountVC: GMSAutocompleteViewControllerDelegate {
    func autocompleteClicked() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue))
        autocompleteController.placeFields = fields
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.txtFldLocation.text = place.name
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

//MARK: - API Calling
extension CreateAccountVC {
    func createAcccountAPICall() {
        if isValidate() {
            let para: [String: Any] = ["firstName": txtFldFirstName.text!, "lastName": txtFldLastName.text!, "email": txtFldEmailAddress.text!, "dob": txtfldDOB.text!, "phone": txtFldMobileNumber.text!, "location": txtFldLocation.text!, "password": txtFldPassword.text!, "confirmPassword": txtFldConfirmPassword.text!, "countryCode": txtFldCountryCode.text!, "allowMail": "false", "loginType": "GENERAL", "deviceToken": pushDeviceToken, "appType": "IOS", "pushMode": "P"]
            
            APIService.postApiCallingWithHeaderMethod(param: para, url: API.createAccount, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    let data = response["response_data"]
                    let success = response["success"].boolValue
                    if success {
                        let userId = data["userId"].stringValue
                        let vc = VerifyOTPVC.getInstance()
                        vc.Id = userId
                        let message = response["message"].stringValue
                        self.view.makeToast(message)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else {
                        let message = response["message"].stringValue
                        ErrorHanding().showAlert(message: message, vc: self)
                    }
                default:
                    let message = response["message"].stringValue
                    ErrorHanding().showAlert(message: message, vc: self)
                }
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
}
