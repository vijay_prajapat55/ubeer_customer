//
//  AddItemToCartPopupView.swift
//  Customer
//
//  Created by Vijay Prajapat on 14/01/22.
//

import UIKit
import CoreData

protocol AddItemToCartPopupViewDelegate: NSObject {
    func addItemToCartPopupViewDismissed()
    func navigateToMyCart()
}

class AddItemToCartPopupView: UIViewController {
    
    @IBOutlet weak var buttonAddToCart: UIButton! {
        didSet {
            buttonAddToCart.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var labelItemCount: UILabel!
    @IBOutlet weak var stackViewItemCounter: UIStackView!
    @IBOutlet weak var labelItemDescription: UILabel!
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var labelItemPrice: UILabel!
    @IBOutlet weak var imageViewItemImage: UIImageView!
    
    var model: Item!
    var itemCount = 1
    var fetchedData = [NSManagedObject]()
    var vendorId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    static func getInstance() -> AddItemToCartPopupView {
        return R.storyboard.orderPlace.addItemToCartPopupView()!
    }
    
    func increaseItemCount() {
        self.itemCount += 1
        DispatchQueue.main.async {
            self.labelItemCount.text = "\(self.itemCount)"
        }
    }
    
    func decreaseItemCount() {
        if self.itemCount != 1 {
            self.itemCount -= 1
            DispatchQueue.main.async {
                self.labelItemCount.text = "\(self.itemCount)"
            }
        }
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddToCartAction(_ sender: Any) {
        let totalPrice = (model.price ?? 0.0) * Double(self.itemCount)
        //    let itemDict: [String: Any] = ["itemId": (model.itemID ?? ""), "name": (model.itemName ?? ""), "menuImage": (model.menuImage ?? ""), "price": (model.price ?? 0.0), "categoryId": (model.categoryID ?? ""), "quantity": self.itemCount, "waitingTime": "", "vendorId": self.vendorId, "totalPrice": totalPrice]
        //  let fd = ItemDetails(dict: itemDict)
        
        //        var model: ItemModel?
        //        let itemdic: [String: Any] = [model?.itemId: (model.itemID ?? ""), model?.name: (model.itemName ?? ""), model?.menuImage: (model?.menuImage ?? ""), model?.price: (model?.price ?? 0.0), model?.categoryId: (model.categoryID ?? ""), model?.quantity: self.itemCount, model?.waitingTime: "", model?.venderId: self.vendorId, model?.totalPrice: totalPrice]
        //        let it = ItemModel(key: "itemId", value: <#T##ItemDetails?#>)
        //    let strItem = dictionaryToString(dict: fd)
        //   saveString(name: strItem)
        saveString(name: (model.itemName ?? ""), itemId: (model.itemID ?? ""), menuImage: (model.menuImage ?? ""), categoryId: (model.categoryID ?? ""), quantity: self.itemCount, waitingTime: "", vendorId: self.vendorId, totalPrice: totalPrice, productId: (model.itemID ?? ""), price: (model.price ?? 0.0))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMinusAction(_ sender: Any) {
        decreaseItemCount()
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        increaseItemCount()
    }
    
    func setupUI() {
        self.imageViewItemImage.sd_setImage(with: URL(string : model.menuImage ?? ""), completed: nil)
        self.labelItemName.text = model.itemName
        self.labelItemPrice.text = "\(model.price ?? 0)"
        self.labelItemDescription.text = model.itemDescription
        self.labelItemCount.text = "\(self.itemCount)"
    }
}

extension AddItemToCartPopupView {
    func saveString(name: String, itemId: String, menuImage: String, categoryId: String, quantity: Int, waitingTime: String, vendorId: String, totalPrice: Double, productId: String, price: Double) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "CartItems", in: managedContext)!
        let item = NSManagedObject(entity: entity, insertInto: managedContext) as! CartItems
        
        item.setValue(name, forKeyPath: "name")
        item.setValue(itemId, forKeyPath: "itemId")
        item.setValue(menuImage, forKeyPath: "menuImage")
        item.setValue(categoryId, forKeyPath: "categoryId")
        item.setValue(quantity, forKeyPath: "quantity")
        item.setValue(waitingTime, forKeyPath: "waitingTime")
        item.setValue(vendorId, forKeyPath: "vendorId")
        item.setValue(totalPrice, forKeyPath: "totalPrice")
        item.setValue(productId, forKeyPath: "productId")
        item.setValue(price, forKeyPath: "price")
        
        do {
            try managedContext.save()
            fetchedData.append(item)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}

extension AddItemToCartPopupView {
    //    func dictionaryToString(dict: ItemDetails) -> String {
    //        let itemDict: [String: Any] = ["itemId": (dict.itemId ?? ""), "name": (dict.name ?? ""), "menuImage": (dict.menuImage ?? ""), "price": (dict.price ?? 0.0), "categoryId": (dict.categoryId ?? ""), "quantity": dict.quantity ?? 0, "waitingTime": "", "vendorId": dict.venderId ?? "", "totalPrice": dict.totalPrice ?? 0.0]
    //        let jsonData = try? JSONSerialization.data(withJSONObject: itemDict, options: [])
    //        let jsonString = String(data: jsonData!, encoding: .utf8)
    //        print(jsonString!)
    //        return jsonString!
    //    }
}
