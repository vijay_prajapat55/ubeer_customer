//
//  Date+Extension.swift
//
//  Created by Vijay Prajapat on 30/12/21.
//

import UIKit
import Foundation

extension Date {
    func convertRequestDateTimeFormetReq() -> String? {
        let dateformet = DateFormatter()
        dateformet.timeZone = TimeZone.current
        dateformet.dateFormat = DateFormat.inspection
        let str = dateformet.string(from: self)
        return str
    }
    
    func addDays(_ numDays: Int) -> Date {
        var components = DateComponents()
        components.day = numDays
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    static func nextDate(value: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: value, to: Date())!
    }
    
    func toDayString() -> String {
        let todayDate = Date().toString(DateFormat.dateFormet)
        let tomorrowDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!.toString(DateFormat.dateFormet)
        let weekDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())!.toString(DateFormat.dateFormet)
        let currentDate = self.toString(DateFormat.dateFormet)
        
        if todayDate == currentDate {
            return "Today"
        } else if tomorrowDate == currentDate {
            return "Tomorrow"
        } else if currentDate.toDate(DateFormat.dateFormet) < weekDate.toDate(DateFormat.dateFormet) {
            return self.toString(DateFormat.dayName)
        } else {
            return self.toString(DateFormat.date)
        }
    }
    
    func toString(_ dateFormate: String) -> String {
        let df = DateFormatter()
        df.dateFormat = dateFormate
        return df.string(from: self)
    }
    
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var startOfMonth: Date {
        
        //let calendar = Calendar(identifier: .gregorian)
        let components = Calendar.current.dateComponents([.year, .month], from: self)
        
        return  Calendar.current.date(from: components)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
    
}
extension String {
    func toDate(_ dateFormate: String) -> Date {
        let df = DateFormatter()
        df.dateFormat = dateFormate
        return df.date(from: self) ?? Date()
    }
}

struct DateFormat {
    static let dayName = "EEEE"
    static let date = "dd-MM-yyyy"
    static var dateFormet:String = "yyyy-MM-dd"
    static let dateTime = "dd-MM-yyyy hh:mm a"
    static var timeFormet:String = "hh:mm a"
    static var time12:String = "h:mm a"
    static var time24:String = "HH:mm"
    static let inspectionTimePropertyDetails = "dd/MM/yyyy HH:mm:ss"
    static let inspection = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let shortDateFormat = "MMM d"
    static var formatForchatbackend = "yyyy-MM-dd HH:mm:ss"
    static var fullDateTimeFormet:String = "dd MMMM, yyyy, hh:mm a"
    static var monthDateTimeFormet:String = "dd MMM, yyyy hh:mm a"
    static var displayDateFormet:String = "dd MMMM, yyyy"
    static var dayDate: String = "MMM dd, yyyy"
    static var year: String = "yyyy"
}
