//
//  SearchVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 07/01/22.
//

import UIKit

class SearchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> SearchVC {
        return R.storyboard.dashboard.searchVC()!
    }
    
}
