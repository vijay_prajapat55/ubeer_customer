//
//  Constants.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import Foundation
import UIKit
import CoreLocation

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

enum FontStyle: String {
    case normal = "CenturyGothic"
    case bold = "CenturyGothic-Bold"
}

struct AppConstants {
    static let googleClientID = "528999963852-vh40k543r0d0vibjm589o3096o856n19.apps.googleusercontent.com"
    static let googlePlacesAPIkey = "AIzaSyA894U-lA4acpK6Y6LEpQ8Dt_YsgGQxq5U"// quikbite key
    static let appType = "IOS"
    static let userType = "CUSTOMER"
    static let guestUserType = "GUEST"
}

struct API {
    // BASE URL
    static let BASE_URL = "https://nodeserver.mydevfactory.com:1655/api/customer/"
    
    // ONBOARDING
    static let sendOtp = BASE_URL + "send-otp"
    static let varifyOtp = BASE_URL + "verify-otp"
    static let vendorDetails = BASE_URL + "vendorDetails"
    static let updateProfile = BASE_URL + "update-profile"
    static let promoCode = BASE_URL + "PromoCode"
    static let checkFare = BASE_URL + "checkFare"
    static let physicalAddressByLatlong = BASE_URL + "physicalAddressByLatlong"
    static let createAccount = BASE_URL + "register"
    static let login = BASE_URL + "login"
    static let forgotPassword = BASE_URL + "forgotPassword"
    static let verifyOTP = BASE_URL + "customerVerifyUser"
    static let resendOTP = BASE_URL + "resendForgotPassOtp"
    static let resetPassword = BASE_URL + "resetPassword"
    static let dashboard = BASE_URL + "dashboard"
    static let addAddress = BASE_URL + "addAddress"
    static let deleteAddress = BASE_URL + "deleteAddress"
    static let listAddress = BASE_URL + "listAddress"
    static let verifyPayment = BASE_URL + "verifyPayment"
    static let postOrder = BASE_URL + "postOrder"
    static let orderList = BASE_URL + "orderList"
    static let viewProfile = BASE_URL + "viewProfile"
    
    static let logout = BASE_URL + "logout"
    
    // MATCHING
    static let userList = BASE_URL + "user-list"
    static let like = BASE_URL + "like"
}

let UserDefault = UserDefaults.standard
struct UserDefaultsKey {
    static let isLogin = "is_login"
    static let authorisationToekn = "AuthorisationToekn"
    static let FCMToken = "FCMToken"
    static let deviceToken = "deviceToken"
    static let userId = "userId"
    static let userAddress = "userAddress"
    static let deliveryLat = "deliveryLat"
    static let deliveryLong = "deliveryLong"
    static let isDefaultAddressSaved = "isDefaultAddressSaved"
    static let customerName = "customerName"
    static let deliveryLatLong = "deliveryLatLong"
    static let customerEmailID = "customerEmailID"
    static let userPhone = "userPhone"
    static let customerLoginType = "customerLoginType"
    static let FName = "FName"
    static let LName = "LName"
    static let profileImage = "profileImage"
    static let email = "email"
    static let houseNumber = "houseNumber"
    static let deliveryAddressType = "deliveryAddressType"
    static let userName = "userName"
    static let userLat = "userLat"
    static let userLong = "userLong"
}

var FCMToken: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.FCMToken) ?? "iOSTestingToken"
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.FCMToken)
    }
}

var name: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.userName) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userName)
    }
}

var pushDeviceToken: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.deviceToken) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.deviceToken)
    }
}

var authorisationToekn: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.authorisationToekn) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.authorisationToekn)
    }
}

var userId: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.userId) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userId)
    }
}

 var customerLoginType: String {
    set {
        UserDefaults.standard.setValue(newValue, forKey: UserDefaultsKey.customerLoginType)
    }
    get {
        return String(describing: UserDefaults.standard.value(forKey: UserDefaultsKey.customerLoginType))
    }
}

 var firstName: String {
    set {
        UserDefaults.standard.setValue(newValue, forKey: UserDefaultsKey.FName)
    }
    get {
        return String(describing: UserDefaults.standard.value(forKey: UserDefaultsKey.FName))
    }
}

 var lastName: String {
    set {
        UserDefaults.standard.setValue(newValue, forKey: UserDefaultsKey.LName)
    }
    get {
        return String(describing: UserDefaults.standard.value(forKey: UserDefaultsKey.LName))
    }
}

 var profileImage: String {
    set {
        UserDefaults.standard.setValue(newValue, forKey: UserDefaultsKey.profileImage)
    }
    get {
        return String(describing: UserDefaults.standard.value(forKey: UserDefaultsKey.profileImage))
    }
}

 var email: String {
    set {
        UserDefaults.standard.setValue(newValue, forKey: UserDefaultsKey.email)
    }
    get {
        return String(describing: UserDefaults.standard.value(forKey: UserDefaultsKey.email))
    }
}

var userPhone: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.userPhone) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userPhone)
    }
}

var userAddress: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.userAddress) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userAddress)
    }
}

var deliveryLat: Double {
    get {
        return UserDefault.double(forKey: UserDefaultsKey.deliveryLat)
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.deliveryLat)
    }
}

var deliveryLong: Double {
    get {
        return UserDefault.double(forKey: UserDefaultsKey.deliveryLong)
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.deliveryLong)
    }
}

var userLong: Double {
    get {
        return UserDefault.double(forKey: UserDefaultsKey.userLong)
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userLong)
    }
}

var userLat: Double {
    get {
        return UserDefault.double(forKey: UserDefaultsKey.userLat)
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.userLat)
    }
}

var isDefaultAddressSaved: Bool {
    get {
        return UserDefault.bool(forKey: UserDefaultsKey.isDefaultAddressSaved)
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.isDefaultAddressSaved)
    }
}

var customerName: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.customerName) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.customerName)
    }
}

var customerEmailID: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.customerEmailID) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.customerEmailID)
    }
}

var houseNumber: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.houseNumber) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.houseNumber)
    }
}

var deliveryAddressType: String {
    get {
        return UserDefault.string(forKey: UserDefaultsKey.deliveryAddressType) ?? ""
    }
    set {
        UserDefault.setValue(newValue, forKey: UserDefaultsKey.deliveryAddressType)
    }
}

class CustomNormalLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        font = AppFont.getFont(style: .normal, size: .small)
    }
}

class CustomBoldFontLabel: UILabel {
  
  override func awakeFromNib() {
      super.awakeFromNib()
      font = AppFont.getFont(style: .normal, size: .small)
  }
}

class CustomLightLabel: UILabel {
  
  override func awakeFromNib() {
      super.awakeFromNib()
      font = AppFont.getFont(style: .normal, size: .small)
  }
}

struct AppStrings {
    
    //Group One
    static let blankString = ""
    static var chooseYourFood: String { return "Choose your Food" }
    static var dummyText: String {
        return "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout." }
    static var getStarted_caps: String { return "GET STARTED" }
    static var skip_caps: String { return "SKIP" }
    static var welcomeTo: String { return "Welcome To" }
    static var app: String { return "App" }
    static var continueWithFacebook: String { return "CONTINUE WITH FACEBOOK" }
    static var continueWithGoogle: String { return "CONTINUE WITH GOOGLE" }
    static var continueWithApple: String { return "CONTINUE WITH APPLE" }
    static var signUpWithEmail: String { return "SIGN UP WITH EMAIL" }
    static var alreadyHaveAnAccount: String { return "Already have an account" }
    static var deliveringTo: String { return "DELIVERING TO" }
    static var selectYourLocation: String { return "Select Your Location" }
    static var selectLocation: String { return "Select Location" }
    static var email: String { return "Email" }
    static var password: String { return "Password" }
    static var forgotPassword: String { return "Forgot Password" }
    static var signIn: String { return "Sign In" }
    static var signUp: String { return "Sign Up" }
    static var signIn_caps: String { return "SIGN IN" }
    static var signUp_caps: String { return "SIGN UP" }
    static var dontHaveAnAccount: String { return "Don't have an account" }
    static var resetPassword: String { return "Reset Password" }
    static var submit_caps: String { return "SUBMIT" }
    static var passwordInstruction: String { return "Passwords should be at least 8 characters long." }
    static var enterYourMobileNumber: String { return "Enter Your Mobile Number" }
    static var youWillGetOTP: String { return "You will get an OTP via SMS" }
    static var sendVerificationCode: String { return "Send Verification Code" }
    static var verifyOTP: String { return "Verify OTP" }
    static var verification: String { return "Verification" }
    static var codeHasBeenSentToYourMobile: String { return "Code has been sent to your mobile" }
    static var resendOTP: String { return "Resend OTP" }
    static var ifOtpIsNotReceived: String { return "(if OTP code is not received)" }
    static var submitCode: String { return "Submit Code" }
    static var createAccount: String { return "Create Account" }
    static var firstName: String { return "First Name" }
    static var lastName: String { return "Last Name" }
    static var location: String { return "Location" }
    static var dob: String { return "DOB" }
    static var mobileNumber: String { return "Mobile Number" }
    static var confirmPassword: String { return "Confirm Password" }
    static var agreeWith: String { return "By signing up, you agree with the" }
    static var tAndC: String { return "Terms of Service" }
    static var and: String { return "&" }
    static var privacyPolicy: String { return "Privacy Policy" }
    static var cancel: String { return "Cancel" }
    static var done: String { return "Done" }
    static var ok: String { return "ok" }
    static var next: String { return "Next" }
    static var home: String { return "Home" }
    static var search: String { return "Search" }
    static var orders: String { return "Orders" }
    static var account: String { return "Account" }
    static var changePassword: String { return "Change Password" }
    static var changeMobileNumber: String { return "Change Mobile Number" }
    static var editProfileCaps: String { return "EDIT PROFILE" }
    static var contact: String { return "Contact" }
    static var myAccount: String { return "My Account" }
    static var favouriteRestaurantsMenu: String { return "Favourite Restaurants" }
    static var notificationSettingsMenu: String { return "Notification Settings" }
    static var addressMenu: String { return "Address" }
    static var paymentMenu: String { return "Payment" }
    static var inviteFriendsMenu: String { return "Invite Friends" }
    static var rateAppMenu: String { return "Rate App" }
    static var helpMenu: String { return "Help" }
    static var languageMenu: String { return "Language" }
    static var termsMenu: String { return "Terms & Conditions" }
    static var tAndCMenu: String { return "Terms & Conditions" }
    static var logoutMenu: String { return "Logout" }
    static var editProfile: String { return "Edit Profile" }
    static var manageProfile: String { return "Manage Profile" }
    static var sort: String { return "Sort" }
    static var delivery: String { return "Delivery" }
    static var pickUp: String { return "Pickup" }
    static var restaurant: String { return "Restaurant" }
    static var address: String { return "Address" }
    static var addNewAddress: String { return "Add New Address" }
    static var notificationSettings: String { return "Notification Settings" }
    static var notifications: String { return "Notifications" }
    static var menu: String { return "Menu" }
    static var checkout: String { return "Checkout" }
    static var noAddressSelected: String { return "No address selected" }
    static var orderDeliveryTo: String { return "Order delivery to" }
    static var eta: String { return "Estimated arrival time" }
    static var deliveryType: String { return "Delivery Type" }
    static var favouriteBars: String { return "Favourite Bars" }
    static var openingTime: String { return "Opening Time" }
    static var restaurantAddress: String { return "Restaurant Address" }
    static var cart: String { return "Cart" }
    
    //Group Two - Placeholders
    static var placeholderFirstName: String { return "Enter First Name" }
    static var placeholderLastName: String { return "Enter Last Name" }
    static var placeholderLocation: String { return "Enter Your Location" }
    static var placeholderDOB: String { return "Enter Your DOB" }
    static var placeholderConfirmPassword: String { return "Confirm Password" }
    static var placeholderEmail: String { return "Enter Your Email" }
    static var placeholderPassword: String { return "Enter Your Password" }
    static var placeholderNewPassword: String { return "Type Your New Password" }
    static var placeholderRetypeNewPassword: String { return "Retype Your New Password" }
    static var placeholderMobileNumber: String { return "Mobile Number" }
    static var placeHolderCountryCode: String { return "Country" }
    
    //Group Three - Error Messages
    static var enterFirstName: String { return "Please Enter First Name" }
    static var enterLastName: String { return "Please Enter Last Name" }
    static var enterLocation: String { return "Please Enter Your Location" }
    static var enterDOB: String { return "Please Enter Your DOB" }
    static var enterConfirmPassword: String { return "Please Enter Confirm Password" }
    static var enterEmail: String { return "Please Enter Your Email" }
    static var enterPassword: String { return "Please Enter Your Password" }
    static var enterNewPassword: String { return "Please Type Your New Password" }
    static var enterRetypeNewPassword: String { return "Please Retype Your New Password" }
    static var enterMobileNumber: String { return "Please Mobile Number" }
    static var enterCountryCode: String { return "Please Country" }
    static var enterOTP: String { return "Please Enter OTP" }
}

