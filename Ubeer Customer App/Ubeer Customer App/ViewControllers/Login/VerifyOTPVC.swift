//
//  VerifyOTPVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 04/01/22.
//

import UIKit
import SwiftyJSON
import Toast_Swift

class VerifyOTPVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppFont.getFont(style: .normal, size: .large)
        }
    }
    @IBOutlet weak var lblVerification: UILabel! {
        didSet {
            lblVerification.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    @IBOutlet weak var lblCodeHasBeenSent: UILabel! {
        didSet {
            lblCodeHasBeenSent.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var btnResendOTP: UIButton! {
        didSet {
            btnResendOTP.layer.cornerRadius = UIScreen.main.bounds.height / 36
            btnResendOTP.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var lblIfCodeIsNotReceived: UILabel! {
        didSet {
            lblCodeHasBeenSent.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var btnNext: UIButton! {
        didSet {
            btnNext.titleLabel?.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFld1: UITextField! {
        didSet {
            txtFld1.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    @IBOutlet weak var txtFld2: UITextField! {
        didSet {
            txtFld2.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    @IBOutlet weak var txtFld3: UITextField! {
        didSet {
            txtFld3.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    @IBOutlet weak var txtFld4: UITextField! {
        didSet {
            txtFld4.font = AppFont.getFont(style: .bold, size: .regular)
        }
    }
    
    var isComeFromForgotPassword = false
    var phoneNumber = ""
    var Id = ""
    var otp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblVerification.font = AppFont.getFont(style: .bold, size: .regular)
        self.txtFld1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.txtFld1.clearsOnBeginEditing = true
        self.txtFld2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.txtFld2.clearsOnBeginEditing = true
        self.txtFld3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.txtFld3.clearsOnBeginEditing = true
        self.txtFld4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.txtFld4.clearsOnBeginEditing = true
    }
    
    static func getInstance() -> VerifyOTPVC {
        return R.storyboard.login.verifyOTPVC()!
    }
    
    func isValidate() -> Bool {
        if txtFld1.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterOTP, vc: self)
            return false
        }
        if txtFld2.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterOTP, vc: self)
            return false
        }
        if txtFld3.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterOTP, vc: self)
            return false
        }
        if txtFld4.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterOTP, vc: self)
            return false
        }
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text ?? ""
        if  text.count >= 1 {
            switch textField {
            case txtFld1:
                txtFld2.becomeFirstResponder()
            case txtFld2:
                txtFld3.becomeFirstResponder()
            case txtFld3:
                txtFld4.becomeFirstResponder()
            case txtFld4:
                txtFld4.resignFirstResponder()
            default:
                break
            }
        }
        if text.count == 0 {
            switch textField {
            case txtFld1:
                txtFld1.becomeFirstResponder()
            case txtFld2:
                txtFld2.becomeFirstResponder()
            case txtFld3:
                txtFld3.becomeFirstResponder()
            case txtFld4:
                txtFld4.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if isComeFromForgotPassword {
            if self.otp == txtFld1.text! + txtFld2.text! + txtFld3.text! + txtFld4.text! {
                self.navigationController?.pushViewController(ResetPasswordVC.getInstance(), animated: true)
            }
        } else {
            verifyOTPAPICall()
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResendOTPAction(_ sender: Any) {
        resendOTPAPICall()
    }
}

//MARK: - API Calling
extension VerifyOTPVC {
    func verifyOTPAPICall() {
        if isValidate() {
            let para: [String: Any] = ["userId": self.Id, "verificationCode": txtFld1.text! + txtFld2.text! + txtFld3.text! + txtFld4.text!, "userType": "customer", "deviceToken": pushDeviceToken, "appType": "IOS", "pushMode": "P"]
            
            APIService.postApiCallingWithHeaderMethod(param: para, url: API.verifyOTP, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    print(response)
                    if self.isComeFromForgotPassword {
                        let vc = ResetPasswordVC.getInstance()
                        vc.phoneNumber = self.phoneNumber
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let data = response["response_data"]
                        let success = response["success"].boolValue
                        if success {
                            authorisationToekn = data["authToken"].stringValue
                            let userDetails = data["userDetails"]
                            customerName = "\(userDetails["firstName"].stringValue) \(userDetails["lastName"].stringValue)"
                            userId = userDetails["id"].stringValue
                            userPhone = userDetails["phone"].stringValue
                            name = userDetails["name"].stringValue
                            customerEmailID = userDetails["email"].stringValue
                            let vc = TabbarVC.getInstance()
                            let message = response["message"].stringValue
                            self.view.makeToast(message)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                default:
                    let message = response["message"].stringValue
                    ErrorHanding().showAlert(message: message, vc: self)
                }
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
    
    func resendOTPAPICall() {
        let para: [String: Any] = ["phone": self.phoneNumber, "userType": "customer", "resendType": pushDeviceToken]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.resendOTP, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                print(response)
                let data = response["response_data"]
                let message = data["message"].stringValue
                self.view.makeToast(message)
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}

