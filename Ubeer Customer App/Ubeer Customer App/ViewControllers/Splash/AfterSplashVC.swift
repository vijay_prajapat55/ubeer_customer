//
//  AfterSplashVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import UIKit
import CoreLocation

class AfterSplashVC: UIViewController {
    
    @IBOutlet weak var lblHeader: UILabel! {
        didSet {
            lblHeader.font = AppFont.getFont(style: .bold, size: .extraLarge)
        }
    }
    @IBOutlet weak var lblSubHeader: UILabel! {
        didSet {
            lblSubHeader.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    @IBOutlet weak var btnGetStarted: UIButton! {
        didSet {
            btnGetStarted.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnGetStarted.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnSkip: UIButton! {
        didSet {
            btnSkip.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
    }
    
    static func getInstance() -> AfterSplashVC {
        return R.storyboard.main.afterSplashVC()!
    }
    
    @IBAction func btnGetStartedClicked(_ sender: UIButton) {
        self.navigationController?.pushViewController(RegisterVC.getInstance(), animated: true)
    }
}
