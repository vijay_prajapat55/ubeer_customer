//
//  SelectCardVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 12/04/22.
//

import UIKit
import Paystack
import Toast_Swift
import CoreData

class SelectCardVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSavedCards: UILabel!
    @IBOutlet weak var tblView: UITableView! {
        didSet {
            tblView.register(UINib(nibName: "SavedCardTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardTableViewCell")
        }
    }
    
    let cardParams = PSTCKCardParams.init()
    var totalAmount = 0
    var vendorId = ""
    var price = 0.0
    var deliveryInstruction = ""
    var items = ""
    var deliveryCost = ""
    var foodServiceCost = ""
    var tipAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.separatorStyle = .none
        cardParams.number = "4084 0800 0000 0409"
        cardParams.cvc = "000"
        cardParams.expYear = 23
        cardParams.expMonth = 04
    }
    
    static func getInstance() -> SelectCardVC {
        return R.storyboard.orderPlace.selectCardVC()!
    }
    
    @IBAction func btnNotificationAction(_ sender: Any) {
        
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayWithNewCardAction(_ sender: Any) {
        let vc = AddCardVC.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // Create Fetch Request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItems")
        let result = try? managedContext.fetch(request)
        let resultData = result as! [NSManagedObject]
        
        for i in resultData {
            managedContext.delete(i)
        }
        
        do {
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func varifyPaymentAPICall(reference: String, isCardSave: Int) {
        let para: [String: Any] = ["reference": reference, "amount": self.totalAmount, "userType": "CUSTOMER", "customerId": userId,  "email": customerEmailID, "isCardSave": isCardSave]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.verifyPayment, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                print(response)
                self.postOrderAPICall(reference: reference)
                let message = response["message"].stringValue
                self.view.makeToast(message)
                self.navigationController?.pushViewController(MyOrdersVC.getInstance(), animated: true)
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
    
    func postOrderAPICall(reference: String) {
        let para: [String: Any] = ["customerId": userId, "vendorId": vendorId, "deliveryFullAddress": userAddress, "deliveryHouseNo": houseNumber, "deliveryPhone": userPhone, "deliveryAddressType": deliveryAddressType, "orderType": "NORMAL", "price": price, "finalPrice": totalAmount, "specialInstruction": deliveryInstruction, "promocodeId": "", "offerId": "", "items": items, "deliveryPreference": "DELIVERY", "latitude": userLat, "longitude": userLong, "userType": "CUSTOMER", "appType": "IOS","paymentReference": "CARD", "guestName": name, "guestCountryCode": "+91", "deliveryCountryCode": "+91", "deliveryLandmark": "", "guestPhone": userPhone, "estimatedDeliveryTime": 0, "deliveryLat": deliveryLat, "deliveryLong": deliveryLong, "deliveryCost": deliveryCost, "foodServiceCost": foodServiceCost, "paymentStatus": "success", "customerPaymentReference": reference, "tipAmount": tipAmount]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.postOrder, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                print(response)
                let data = response["response_data"]
                let message = data["message"].stringValue
                self.view.makeToast(message)
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}

extension SelectCardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardTableViewCell", for: indexPath) as? SavedCardTableViewCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        payFromSelectedCard()
    }
    
    func payFromSelectedCard() {
        // cardParams already fetched from our view or assembled by you
        let transactionParams = PSTCKTransactionParams.init()
        
        // building new Paystack Transaction
        transactionParams.amount = UInt(Int(self.totalAmount))
        transactionParams.currency = "ZAR"
        //           let custom_filters: NSMutableDictionary = [
        //               "recurring": true
        //           ]
        //           let items: NSMutableArray = [
        //               "Bag","Glasses"
        //           ]
        //           do {
        //               try transactionParams.setCustomFieldValue("iOS SDK", displayedAs: "Paid Via")
        //               try transactionParams.setCustomFieldValue("Paystack hats", displayedAs: "To Buy")
        //               try transactionParams.setMetadataValue("iOS SDK", forKey: "paid_via")
        //               try transactionParams.setMetadataValueDict(custom_filters, forKey: "custom_filters")
        //               try transactionParams.setMetadataValueArray(items, forKey: "items")
        //           } catch {
        //               print(error)
        //           }
        transactionParams.email = customerEmailID
        
        // check https://developers.paystack.co/docs/split-payments-overview for details on how these work
        // transactionParams.subaccount  = "ACCT_80d907euhish8d";
        // transactionParams.bearer  = "subaccount";
        // transactionParams.transaction_charge  = 280;
        
        // if a reference is not supplied, we will give one
        // transactionParams.reference = "ChargedFromiOSSDK@"
        
        PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self,
                                           didEndWithError: { (error, reference) -> Void in
            print(error)
            self.view.makeToast(error.localizedDescription)
        }, didRequestValidation: { (reference) -> Void in
            print("request validation")
        }, didTransactionSuccess: { (reference) -> Void in
            self.deleteData()
            let alert = UIAlertController(title: "Alert", message: "Do you want to save this card?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: { action in
                print("d")
                self.varifyPaymentAPICall(reference: reference, isCardSave: 1)
            })
            let cancelAction = UIAlertAction(title: "No", style: .default) { action in
                print("s")
                self.varifyPaymentAPICall(reference: reference, isCardSave: 0)
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            print("payment success")
        })
    }
}
