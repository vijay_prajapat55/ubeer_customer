//
//  DashboardCategoryContainerTableViewCell.swift
//  Customer
//
//  Created by Pradipta on 16/11/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class DashboardCategoryContainerTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewCategory: UICollectionView!
       
    var model: [CategoryDatum]?
    var baseUrl = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionViewCategory.register(UINib(nibName: "DashboardCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DashboardCategoryCollectionViewCell")
        self.collectionViewCategory.dataSource = self
        self.collectionViewCategory.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() {
        self.collectionViewCategory.reloadData()
    }
}

extension DashboardCategoryContainerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.count ?? 0
        //self.presenter?.arrayCategory.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCategoryCollectionViewCell", for: indexPath) as? DashboardCategoryCollectionViewCell else { return UICollectionViewCell() }
      //  cell.imageViewCategory.image = R.image.beer()!

        let imageUrl = "\(self.baseUrl)\(self.model?[indexPath.row].image ?? "")"
        cell.imageViewCategory.sd_setImage(with: URL(string : imageUrl), placeholderImage: UIImage(named: "Place holder Images")) { (img, err, type, url) in
        }
        cell.imageViewCategory.contentMode = .scaleAspectFill
        cell.labelCategory.isHidden = false
        cell.labelCategory.text = self.model?[indexPath.row].categoryName
        //self.presenter?.arrayCategory[indexPath.item].categoryName ?? ""
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //height : width = 4 : 3
        return CGSize(width: collectionView.bounds.height * 0.75, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  self.categoryTapped?(self, self.presenter?.arrayCategory[indexPath.row])
    }
}
