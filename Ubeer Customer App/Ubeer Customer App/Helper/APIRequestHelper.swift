//
//  APIRequestHelper.swift
//

import Alamofire
import SystemConfiguration
import SVProgressHUD
import SwiftyJSON
import UIKit

//MARK:- Variables
enum ContentType: String {
    case applicationJson = "application/json"
    case applicationFormURL = "application/x-www-form-urlencoded"
}

func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
}

//MARK:-
class ApiRequestHelper {
    
    static func requestWebService(url: String, methodType: HTTPMethod, requireAuthorisation: Bool = false,
                                  parameters: [String: Any]? = nil,
                                  showProgress: Bool = true,
                                  vc: UIViewController,
                                  success: @escaping(_ response: JSON, _ isSuccess: Bool, _ code: Int) -> Void,
                                  failure: @escaping(_ error: Error) -> Void){
        if !isConnectedToNetwork() {
            return
        }
        
        if showProgress{
            DispatchQueue.main.async {
                SVProgressHUD.setBackgroundColor(.darkGray)
                SVProgressHUD.resetOffsetFromCenter()
                SVProgressHUD.show()
            }
        }
        
        let headers: HTTPHeaders? = requireAuthorisation
        ? ["Accept": ContentType.applicationJson.rawValue, "Authorization": "Bearer \(authorisationToekn)"]
        : ["Accept": ContentType.applicationJson.rawValue]
        print("URL: \(url)")
        print("Parameter: \(JSON(parameters ?? [:]))")
        AF.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: methodType, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if response.response?.statusCode == 404 {
                    let data = JSON(response.data ?? Data())
                    print("Response: \(data)")
                    return
                }
                switch response.result {
                case let .success(value):
                    let data = JSON(value)
                    let statusCode = response.response?.statusCode
                    print("Response: \(data)")
                    success(data, ErrorHanding().isSuccess(statusCode), statusCode!)
                    break
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                    failure(error)
                }
            }
    }
    
    static func requestEncodingWebService(url : String, methodType : HTTPMethod,
                                          requireAuthorisation : Bool = true,
                                          parameters : [String : Any]?,
                                          showProgress : Bool = false,
                                          success: @escaping(_ response : JSON, _ isSuccess : Bool) -> Void,
                                          failure: @escaping(_ error : Error) -> Void){
        if !isConnectedToNetwork() {
            return
        }
        
        if showProgress{
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
        }
        let headers : HTTPHeaders? = requireAuthorisation
        ? ["Accept": ContentType.applicationJson.rawValue]
        : ["Accept": ContentType.applicationJson.rawValue]
        
        AF.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if response.response?.statusCode == 404 {
                    let data = JSON(response.data ?? Data())
                    print("Response: \(data)")
                    return
                }
                switch response.result {
                case let .success(value):
                    let data = JSON(value)
                    let statusCode = response.response?.statusCode
                    success(data, ErrorHanding().isSuccess(statusCode))
                    break
                case .failure(let error):
                    failure(error)
                }
            }
    }
    
    static func requestWebServiceWithImage(images: [UIImage],
                                           photoParamName : [String],
                                           url : String,
                                           requireAuthorisation : Bool = true,
                                           parameters : [String : Any]?,
                                           showProgress : Bool = false,
                                           success : @escaping (_ response : JSON, _ isSuccess : Bool) -> (),
                                           failure : @escaping(_ error : Error) -> (),
                                           progressHandler:@escaping(_ progress: Double)->()){
        if !isConnectedToNetwork() {
            return
        }
        
        if showProgress{
            SVProgressHUD.show()
        }
        
        let headers : HTTPHeaders? = requireAuthorisation
        ? ["Accept": ContentType.applicationJson.rawValue, "Authorization": "Bearer \(authorisationToekn)"]
        : ["Accept": ContentType.applicationJson.rawValue]
        
        AF.upload(multipartFormData: { multipartFormData in
            for i in 0..<images.count {
                let rotatedImage = images[i]
                if let imgData = rotatedImage.jpegData(compressionQuality: 1){
                    multipartFormData.append(imgData, withName: photoParamName[i], fileName: "0\(i).jpeg", mimeType: "image/jpeg")
                }
            }
            if let params = parameters{
                for (key, value) in params {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    else if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    else if let temp = value as? [String : Any] ,
                            let data = try? JSONSerialization.data(withJSONObject: temp, options: .prettyPrinted) as Data{
                        multipartFormData.append(data, withName: key)
                    }
                    else if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            }
        },to: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, usingThreshold: UInt64.init(),
                  method: .post,
                  headers: headers).uploadProgress(queue: .main, closure: { (progress) in
            
            progressHandler(progress.fractionCompleted)
            
        }).responseJSON{ response in
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            if response.response?.statusCode == 404 {
                let data = JSON(response.data ?? Data())
                print("Response: \(data)")
                return
            }
            switch response.result {
            case let .success(value):
                let data = JSON(value)
                let statusCode = response.response?.statusCode
                success(data, ErrorHanding().isSuccess(statusCode))
                break
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    static func cancelOngoingRequest(url: String) {
        AF.session.getAllTasks { (tasks) in
            tasks.forEach {
                if ($0.originalRequest?.url?.absoluteString == url) {
                    $0.cancel()
                }
            }
        }
    }
}
