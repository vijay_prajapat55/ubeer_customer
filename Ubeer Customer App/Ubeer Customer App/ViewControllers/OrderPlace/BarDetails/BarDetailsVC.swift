//
//  BarDetailsVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 13/01/22.
//

import UIKit
import CoreLocation

class BarDetailsVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblAddress: CustomNormalLabel! {
        didSet {
            lblAddress.font = AppFont.getFont(style: .normal, size: .large)
        }
    }
    @IBOutlet weak var tableViewMenuItems: UITableView!
    @IBOutlet weak var imgViewBar: UIImageView!
    @IBOutlet weak var lblBarName: CustomNormalLabel! {
        didSet {
            lblBarName.font = AppFont.getFont(style: .bold, size: .small)
        }
    }
    @IBOutlet weak var lblBarAddress: CustomNormalLabel! {
        didSet {
            lblBarAddress.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var lblTime: CustomNormalLabel! {
        didSet {
            lblTime.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var lblEstimatedTime: CustomNormalLabel! {
        didSet {
            lblEstimatedTime.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var lblRating: CustomNormalLabel! {
        didSet {
            lblRating.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    
    //MARK: - Other Variables
    var lat = CLLocationDegrees()
    var lng = CLLocationDegrees()
    var address = ""
    var vendorId = ""
    var model: BarDetailsModel?
    private var isRestaurantClosed = true
    private var selectedSort: String = ""
    
    //MARK: - ViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewMenuItems.register(UINib(nibName: "RestaurantMenuItemsDisplayTableViewCell", bundle: nil), forCellReuseIdentifier: "RestaurantMenuItemsDisplayTableViewCell")
        self.tableViewMenuItems.backgroundColor = .white
        self.tableViewMenuItems.dataSource = self
        self.tableViewMenuItems.delegate = self
        barDetailsAPICalling()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AddItemToCartPopupView.getInstance().dismiss(animated: true, completion: nil)
    }
    
    static func getInstance() -> BarDetailsVC {
        return R.storyboard.orderPlace.barDetailsVC()!
    }
    
    func setDesign(model: Restaurant) {
        self.lblAddress.text = userAddress
        self.imgViewBar.sd_setImage(with: URL(string : model.banner ?? ""), placeholderImage: UIImage(named: "DummyLogo")) { (img, err, type, url) in}
        self.lblBarName.text = model.name
        self.lblBarAddress.text = model.address
        self.lblRating.text = "\(model.rating ?? 0)"
    }
    
    //MARK: - IBAction
    @IBAction func btn_BackDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BarDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model?.responseData?.catItem?.item.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantMenuItemsDisplayTableViewCell", for: indexPath) as? RestaurantMenuItemsDisplayTableViewCell else {
            return UITableViewCell()
        }
        cell.setItemData(model: (self.model?.responseData?.catItem?.item[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AddItemToCartPopupView.getInstance()
        vc.model = self.model?.responseData?.catItem?.item[indexPath.row]
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.vendorId = self.model?.responseData?.restaurant?.id ?? ""
        self.present(vc, animated: true, completion: nil)
    }
}

extension BarDetailsVC {
    func barDetailsAPICalling() {
        let para: [String: Any] = ["customerId": userId, "categoryId": "1", "latitude": self.lat, "longitude": self.lng, "userType": "CUSTOMER", "sort": "POPULARITY", "vendorId": self.vendorId, "restaurantInfo": "YES"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.vendorDetails, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                self.model = BarDetailsModel(fromDictionary: response)
                self.setDesign(model: (self.model?.responseData?.restaurant)!)
                self.tableViewMenuItems.reloadData()
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}

