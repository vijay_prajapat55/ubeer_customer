//
//  BannersCollectionViewCell.swift
//
//  Created by Souvik on 23/03/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class BannersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image_Banner: UIImageView!
    @IBOutlet weak var labelRestaurantNames: CustomBoldFontLabel!
    @IBOutlet weak var labelOfferText: CustomNormalLabel!
    @IBOutlet weak var offerTextheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var restaurantNameHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
