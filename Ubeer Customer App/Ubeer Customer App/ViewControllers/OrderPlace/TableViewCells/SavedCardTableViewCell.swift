//
//  SavedCardTableViewCell.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 12/04/22.
//

import UIKit

class SavedCardTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblCardDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
