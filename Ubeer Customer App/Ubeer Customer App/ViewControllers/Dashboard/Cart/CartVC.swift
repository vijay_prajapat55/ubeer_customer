//
//  CartVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 17/01/22.
//

import UIKit
import CoreData
import SwiftyJSON

class CartVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView! {
        didSet {
            tblView.register(UINib(nibName: "CartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "CartItemTableViewCell")
            tblView.addObserver(self, forKeyPath: "contentSize", options: .new ,context: nil)
        }
    }
    @IBOutlet weak var constraintHeightTableView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightView: NSLayoutConstraint!
    @IBOutlet weak var subTotalView: UIView!
    @IBOutlet weak var lblSubTotalAmount: UILabel!
    @IBOutlet weak var lblAddress: CustomNormalLabel!
    
    var itemsArray: [[String: Any]]? = []
    var fetchedData = [NSManagedObject]()
    var itemCount = 0
    var subTotal = 0.0
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> CartVC {
        return R.storyboard.dashboard.cartVC()!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataFetch()
        self.lblAddress.text = userAddress
        self.subTotalView.isHidden = itemsArray?.count ?? 0 == 0 ? true : false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if object is UITableView {
                if let newValue  = change?[.newKey] {
                    let newsize = newValue as! CGSize
                    self.constraintHeightTableView.constant = newsize.height
                    self.constraintHeightView.constant = 360 + newsize.height
                }
            }
        }
    }
    
    @IBAction func btnAddMoreDrinkAction(_ sender: Any) {
        APP_DELEGATE.setupDashboardRoot()
    }
    
    @IBAction func btnProceedToCheckoutAction(_ sender: Any) {
        if self.itemsArray?.count == 0 {
            return
        }
        self.navigationController?.pushViewController(CheckoutVC.getInstance(), animated: true)
    }
}

extension CartVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemTableViewCell", for: indexPath) as? CartItemTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        if let model = itemsArray?[indexPath.row] {
            cell.lblItemName.text = model["name"] as? String
            cell.lblPrice.text = "\(model["price"] as? Double ?? 0.0)"
            //self.itemCount = model["quantity"] as! Int
            cell.lblQuantity.text = "\(model["quantity"] as! Int)"
            cell.itemCount = model["quantity"] as! Int
            cell.btnRemove.tag = indexPath.row
        }
        return cell
    }
}

extension CartVC {
    func dataFetch() {
        self.itemsArray?.removeAll()
        self.subTotal = 0.0
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CartItems")
        do {
            let myData = try managedContext.fetch(fetchRequest)
            for data in myData {
                let name = data.value(forKey: "name") as? String ?? ""
                let itemId = data.value(forKey: "itemId") as? String ?? ""
                let menuImage = data.value(forKey: "menuImage") as? String ?? ""
                let categoryId = data.value(forKey: "categoryId") as? String ?? ""
                let quantity = data.value(forKey: "quantity") as? Int ?? 0
                let waitingTime = data.value(forKey: "waitingTime") as? String ?? ""
                let vendorId = data.value(forKey: "vendorId") as? String ?? ""
                let productId = data.value(forKey: "productId") as? String ?? ""
                let totalPrice = data.value(forKey: "totalPrice") as? Double ?? 0.0
                let price = data.value(forKey: "price") as? Double ?? 0.0
                
                let dict: [String: Any] = ["name": name, "itemId": itemId, "menuImage": menuImage, "quantity": quantity, "categoryId": categoryId, "waitingTime": waitingTime, "vendorId": vendorId, "productId": productId, "totalPrice": totalPrice, "price": price]
                self.itemsArray?.append(dict)
            }
            for item in itemsArray! {
                self.subTotal += (item["price"] as? Double ?? 0.0) * Double(item["quantity"] as! Int)
            }
            let roundedSubTotalAmount = "$\((self.subTotal * 100).rounded() / 100)"
            self.lblSubTotalAmount.text = roundedSubTotalAmount
            tblView.reloadData()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteData(index: Int) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // Create Fetch Request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItems")
        let result = try? managedContext.fetch(request)
        let resultData = result as! [NSManagedObject]
        
        managedContext.delete(resultData[index])
        
        do {
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
    }
}

extension CartVC: CartItemProtocol {
    func ItemsCount(count: Int, cell: UITableViewCell, isPlus: Bool) {
      //  self.itemCount = count
        let index = tblView.indexPath(for: cell)
        let cartCell = cell as! CartItemTableViewCell
        cartCell.lblQuantity.text = String(count)
        
        let item = itemsArray?[index!.row]
        if isPlus {
            self.subTotal += (item?["price"] as? Double ?? 0.0) * 1.0
        } else {
            self.subTotal -= (item?["price"] as? Double ?? 0.0) * 1.0
        }
        let roundedSubTotalAmount = "$\((self.subTotal * 100).rounded() / 100)"
        self.lblSubTotalAmount.text = roundedSubTotalAmount
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CartItems")
        do {
            let myData = try managedContext.fetch(fetchRequest)
            myData[index!.row].setValue(count, forKey: "quantity")
            
            do {
                try managedContext.save()
            } catch _ {
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func removeItem(cell: UITableViewCell) {
        let index = tblView.indexPath(for: cell)!.row

        deleteData(index: index)
        self.itemsArray?.remove(at: index)
        self.tblView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        self.tblView.reloadData()
        self.dataFetch()
    }
}
