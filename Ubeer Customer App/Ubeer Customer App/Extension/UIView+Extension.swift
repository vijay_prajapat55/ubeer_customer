//
//  UIView+Extension.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import Foundation
import UIKit

enum ViewSide {
    case Left, Right, Top, Bottom
}
enum VerticalLocation: String {
    case bottom
    case top
}

class CustomButton: UIButton {
  
  var params: Dictionary<String, Any>
  
  override init(frame: CGRect) {
      self.params = [:]
      super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
      self.params = [:]
      super.init(coder: aDecoder)
  }
  
}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 2)
        layer.shadowRadius = 2
    }
    
    func addShadow(location: VerticalLocation, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        switch location {
        case .bottom:
            addShadow(offset: CGSize(width: 0, height:10), color: color, opacity: opacity, radius: radius)
        case .top:
            addShadow(offset: CGSize(width: 0, height: -10), color: color, opacity: opacity, radius: radius)
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        self.layer.addSublayer(border)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadow_Color: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var maskToBound: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    class func initFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 10.0),
                   shadowOpacity: Float = 0.8,
                   shadowRadius: CGFloat = 5.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func setRoundedCornered(height : CGFloat){
        self.layer.cornerRadius = 8//height/5
    }
    
    func setRounded(height : CGFloat){
        self.layer.cornerRadius = height/2
    }
    
    func setCircularCorner() {
        self.layer.cornerRadius = self.bounds.height / 2
    }
    
    func getAspectRatioHeight(currentWidth: CGFloat, currentHeight: CGFloat) -> CGFloat {
        return (currentHeight / currentWidth) * self.bounds.width
    }
    
    func addCornerRadiusShadowBorder(cornerRadius: CGFloat = 5.0, borderWidth: CGFloat = 1.0, borderColor: UIColor = .black, shadowColor: UIColor = .black, shadowOffset: CGSize = CGSize(width: 2, height: 2), shadowOpacity: Float = 0.4, shadowRadius: CGFloat = 4.0) {
        // corner radius
        self.layer.cornerRadius = cornerRadius
        
        // border
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        
        // shadow
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
    }
}

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isValidImageURL() -> Bool {
        if self.hasPrefix("http") {
            if self.hasSuffix(".png") || self.hasSuffix(".jpg") || self.hasSuffix(".jpeg") { // true
                return true
            }
        }
        return false
    }
}

class NormalButton: UIButton {
  
  override func awakeFromNib() {
      super.awakeFromNib()
      imageView?.contentMode = .scaleAspectFit
      titleLabel?.font = AppFonts.getFont(fontFamily: AppFontFamily.kFontRegularName_1, size: fontSize)
      
  }
  
  @IBInspectable var fontSize: CGFloat = 17.0 {
      didSet {
          titleLabel?.font = AppFonts.getFont(fontFamily: AppFontFamily.kFontRegularName_1, size: fontSize)
      }
  }
  
  @IBInspectable var bgColor: UIColor? {
      didSet {
          backgroundColor = bgColor
      }
  }
  
  override var isHighlighted: Bool {
      didSet {
          if isHighlighted{
              backgroundColor = backgroundColor?.withAlphaComponent(0.6)
          }else{
              backgroundColor = backgroundColor?.withAlphaComponent(1.0)
          }
      }
  }
}

struct AppFontFamily {
    static let kFontRegularName_1 = "CenturyGothic"//"Century Gothic"
    static let kFontMediumName_1 = "CenturyGothic"//"Century Gothic"
    static let kFontLightName_1 = "CenturyGothic"//"Century Gothic"
    static let kFontBoldName_1 = "CenturyGothic-Bold"//"Century Gothic"
    static let kFontHelvetica_1 = "Helvetica"
}

struct AppFonts {
    static func getFont(fontFamily: String, size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: fontFamily, size: size)!// ?? UIFont.systemFont(ofSize: size)
    }
}

extension UIViewController {
    public func showAlertWithTitleOkCancelAction(_ title:String?, message:String?, okTitle: String?, cancelTitle: String?, okAction: (() -> Void)? = nil, cancelAction:(() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle ?? "OK", style: .default) { (result : UIAlertAction) -> Void in
            okAction?()
        }
        let cancelAction = UIAlertAction(title: cancelTitle ?? "Cancel", style: .default) { (result : UIAlertAction) -> Void in
            cancelAction?()
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func showAlertWithTitleOkAction(_ title: String?, message: String?, okTitle: String, okAction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle , style: .default) { (result : UIAlertAction) -> Void in
            okAction?()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
