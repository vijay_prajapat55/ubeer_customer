//
//  RegisterVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import UIKit

class RegisterVC: UIViewController {
    
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var lblHeader: UILabel! {
        didSet {
            lblHeader.font = AppFont.getFont(style: .bold, size: .extraHuge)
        }
    }
    @IBOutlet weak var btnFacebook: UIButton! {
        didSet {
            btnFacebook.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnFacebook.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnGoogle: UIButton! {
        didSet {
            btnGoogle.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnGoogle.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnApple: UIButton! {
        didSet {
            btnApple.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnApple.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnEmail: UIButton! {
        didSet {
            btnEmail.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnEmail.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    @IBOutlet weak var btnSignIn: UIButton! {
        didSet {
            btnSignIn.titleLabel?.font = AppFont.getFont(style: .bold, size: .small)
        }
    }
    @IBOutlet weak var lblSignIn: UILabel! {
        didSet {
            lblSignIn.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGIF()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    static func getInstance() -> RegisterVC {
        return R.storyboard.registration.registerVC()!
    }
    
    func setupGIF() {
        let gif = UIImage.gifImageWithName("Beer-glass-Gif")
        gifImageView.image = gif
    }

    @IBAction func btnSignupWithEmailAction(_ sender: Any) {
        self.navigationController?.pushViewController(CreateAccountVC.getInstance(), animated: true)
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
        self.navigationController?.pushViewController(LoginVC.getInstance(), animated: true)
    }
}
