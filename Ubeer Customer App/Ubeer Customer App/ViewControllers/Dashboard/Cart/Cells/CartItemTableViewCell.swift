//
//  CartItemTableViewCell.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 20/01/22.
//

import UIKit

protocol CartItemProtocol {
    func ItemsCount(count: Int, cell: UITableViewCell, isPlus: Bool)
    func removeItem(cell: UITableViewCell)
}

class CartItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    
    var itemCount = 0
    var delegate: CartItemProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.addCornerRadiusShadowBorder(
            cornerRadius: 5.0,
            borderWidth: 0.0,
            borderColor: .clear,
            shadowColor: .lightGray)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnPlusAction(_ sender: UIButton) {
        itemCount += 1
        self.lblQuantity.text = "\(itemCount)"
        self.delegate?.ItemsCount(count: self.itemCount, cell: self, isPlus: true)
    }
    
    @IBAction func btnMinusAction(_ sender: UIButton) {
        itemCount -= 1
        
        if itemCount == 0 {
            delegate?.removeItem(cell: self)
            return
        }
        self.lblQuantity.text = "\(itemCount)"
        self.delegate?.ItemsCount(count: self.itemCount, cell: self, isPlus: false)
    }
    
    @IBAction func btnRemoveAction(_ sender: UIButton) {
        delegate?.removeItem(cell: self)
    }
    
}
