//
//  SelectLocationVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 16/02/22.
//

import UIKit

class SelectLocationVC: UIViewController {

    @IBOutlet weak var tblView: UITableView! {
        didSet {
            tblView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")
        }
    }
    
    var addressModel: AddressModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAddressListPICalling()
    }
    
    static func getInstance() -> SelectLocationVC {
        return R.storyboard.orderPlace.selectLocationVC()!
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewAddressAction(_ sender: Any) {
        self.navigationController?.pushViewController(AddAddressVC.getInstance(), animated: true)
    }
}

extension SelectLocationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard addressModel != nil else {
            return 0
        }
        return self.addressModel?.responseData?.address?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell", for: indexPath) as? AddressTableViewCell else {
            return UITableViewCell()
        }
        let model = self.addressModel!.responseData!.address![indexPath.row]
        cell.lblAddress.text = model.fullAddress
        cell.lblAddressType.text = model.addressType
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.addressModel?.responseData?.address?[indexPath.row]
        userAddress = model?.fullAddress ?? ""
        deliveryLat = Double(model?.latitude ?? "")!
        deliveryLong = Double(model?.longitude ?? "")!
        let cell = tblView.cellForRow(at: indexPath) as? AddressTableViewCell
        cell?.btnTickMark.setImage(R.image.squareRadioButtonSelected()!, for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.navigationController?.popViewController(animated: true)
        }
    }
} 

extension SelectLocationVC {
    func getAddressListPICalling() {
        let para: [String: Any] = ["customerId": userId, "userType": "CUSTOMER"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.listAddress, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                print(response)
                self.addressModel = AddressModel(fromDictionary: response)
                self.tblView.reloadData()
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
    
    func deleteAddressAPICalling() {
        let para: [String: Any] = ["customerId": userId, "userType": "CUSTOMER", "addressId": ""]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.deleteAddress, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                    
                print(response)
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}
