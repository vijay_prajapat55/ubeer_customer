//
//  SideMenuVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 18/05/22.
//

import UIKit
import SDWebImage
import JGProgressHUD

func IS_IPAD() -> Bool {
    switch UIDevice.current.userInterfaceIdiom {
    case .phone:
        return false
    case .pad:
        return true
    case .unspecified:
        return false
    default:
        return false
    }
}

struct MenuItem {
    var iconName: String?
    var title: String
    var subItems: [MenuSubItem]
    var isExpanded: Bool
    var hasSubItems: Bool
    var isSubItem: Bool
    var menuItemIndexOfSubItem: Int
    
    init() {
        self.iconName = ""
        self.title = ""
        self.subItems = []
        self.isExpanded = false
        self.hasSubItems = false
        self.isSubItem = false
        self.menuItemIndexOfSubItem = -1
    }
    
    init(iconName: String?, title: String, subItems: [MenuSubItem], isExpanded: Bool, hasSubItems: Bool, isSubItem: Bool, menuItemIndexOfSubItem: Int = -1) {
        self.iconName = iconName
        self.title = title
        self.subItems = subItems
        self.isExpanded = isExpanded
        self.hasSubItems = hasSubItems
        self.isSubItem = isSubItem
        self.menuItemIndexOfSubItem = menuItemIndexOfSubItem
    }
    
}

protocol ViewControllerProtocol: class {
    func dataStartedLoading()
    func dataFinishedLoading()
    func showErrorAlertWithTitle(title: String, message: String)
    func showSuccessAlertWithTitle(title: String, message: String)
    func userUnauthorized()
    func userForbidden()
}

struct MenuSubItem {
    var iconName: String?
    var title: String
    
    init() {
        iconName = ""
        title = ""
    }
    
    init(iconName: String?, title: String) {
        self.iconName = iconName
        self.title = title
    }
}

protocol SideMenuVCDelegate: class {
    func clickedMenuHeader()
    func clickedIndexPath(index: Int)
    func clickedLogout()
}

class SideMenuVC: UIViewController {

    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var leftMenuTableView: UITableView!

    @IBOutlet weak var imageViewUserImage: UIImageView!
    @IBOutlet weak var labelUserName: CustomNormalLabel!
    @IBOutlet weak var labelUserPhone: CustomNormalLabel!
    @IBOutlet weak var buttonMenuHeader: UIButton!
    @IBOutlet weak var constraintHeightUserView: NSLayoutConstraint!
    
    var viewController: UIViewController?
    var menuArray: [MenuItem] = []
    let cellHeight: CGFloat = IS_IPAD() ? 75 : 60
    var menuItemsCount: Int = 0
    
    private let hud = JGProgressHUD(style: .light)
    
    public static weak var delegate: SideMenuVCDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.7, y: 0)
        self.alphaView.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeMenu))
        tapGesture.numberOfTapsRequired = 1
        self.alphaView.addGestureRecognizer(tapGesture)
        
        self.menuArray = [
            MenuItem(iconName: "favourite", title: AppStrings.favouriteBars, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "notification", title: AppStrings.notificationSettings, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "orders", title: AppStrings.orders, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "address", title: AppStrings.address, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "payment", title: AppStrings.paymentMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "inviteFriends", title: AppStrings.inviteFriendsMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "rateApp", title: AppStrings.rateAppMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "rateApp", title: AppStrings.helpMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "ic_language", title: AppStrings.languageMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "terms&Condition", title: AppStrings.termsMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false),
            MenuItem(iconName: "logout", title: AppStrings.logoutMenu, subItems: [], isExpanded: false, hasSubItems: false, isSubItem: false)
        ]
        
        self.menuItemsCount = self.menuArray.count
        
        leftMenuTableView.delegate = self
        leftMenuTableView.dataSource = self
        leftMenuTableView.reloadData()
        
        self.setupSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      myProfileDetailAPICall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        constraintHeightUserView.constant = UIScreen.main.bounds.height * 0.34
    }
    
    public static func setDelegate(_ callingClass: SideMenuVCDelegate) {
        self.delegate = callingClass
    }
    
    private func setupSubviews() {
        self.leftMenuTableView.bounces = false
        self.leftMenuTableView.backgroundColor = .clear
      //  self.imageViewUserImage.makeCircular()
        //self.imageViewUserImage.layer.borderColor = UIColor(named: "TextColorLightGray")?.cgColor ?? UIColor.lightGray.cgColor
        //self.imageViewUserImage.layer.borderWidth = 2.0
        self.imageViewUserImage.contentMode = .scaleAspectFill
        //self.imageViewUserImage.dropShadow(color: .lightGray, offSet: CGSize(width: 0.0, height: 0.0))
        //self.refreshSubviews()
    }
    
    // MARK: Menu Closing
    @objc func closeMenu() {
        self.menuClosingGlobal()
    }
    
    func menuClosingGlobal() {
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.8, y: 0)
            self.alphaView.alpha = 0
        }) { (_) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    func menuClosingGlobal(completion: @escaping (_ success: Bool) -> Void) {
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.8, y: 0)
            self.alphaView.alpha = 0
        }) { [weak self] (success) in
            guard let this = self else {
                return
            }
            this.view.removeFromSuperview()
            this.removeFromParent()
            completion(true)
        }
    }
    
    // MARK: Menu push
    func menuClosingForPush(withIndexPath indexPathForRow: IndexPath) {
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.8, y: 0)
            self.alphaView.alpha = 0
        }) { [unowned self] (_) in
            self.view.removeFromSuperview()
            self.removeFromParent()
            self.push(withIndexPath: indexPathForRow)
        }
    }
    
    func push(withIndexPath indexPath: IndexPath) {
        let viewControllers: [UIViewController] = viewController!.navigationController!.viewControllers as [UIViewController];
        let viewControllerCurrent: UIViewController = viewControllers[viewControllers.count - 1]
        switch indexPath.row {
        case 0, 1, 2, 3, 4, 5:
            if viewControllerCurrent.isKind(of: DashboardVC.self) {
                guard let delegate = SideMenuVC.delegate else {
                    return
                }
                delegate.clickedIndexPath(index: indexPath.row)
            } else {
                let homeObj = DashboardVC.getInstance()
                homeObj.hidesBottomBarWhenPushed = true
                viewController!.navigationController?.pushViewController(homeObj, animated: true)
            }
        default:
            let homeObj = DashboardVC.getInstance()
            //homeObj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(homeObj, animated: true)
        }
    }
    
    // MARK: Show Left Menu
    static func showLeftMenu(onParentViewController parentViewController: UIViewController, selected: @escaping (_ value: AnyObject?, _ index: Int?) -> Void) {
        let menuVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        menuVC.showMenu(onParentViewController: parentViewController)
    }
    
    func showMenu(onParentViewController parentViewController: UIViewController) {
        self.view.frame = UIScreen.main.bounds
        UIApplication.shared.windows.first!.addSubview(self.view)
        parentViewController.addChild(self)
        self.didMove(toParent: parentViewController)
        parentViewController.view.bringSubviewToFront(self.view)
        
        viewController = parentViewController
        
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = .identity
            self.alphaView.alpha = 0.7
        }) { (_) in
            
        }
    }
    
    @IBAction func buttonMenuHeaderClicked(_ sender: UIButton) {
        self.menuClosingGlobal { (success) in
            guard let delegate = SideMenuVC.delegate else {
                return
            }
            delegate.clickedMenuHeader()
        }
    }
    
    @IBAction func buttonCloseClicked(_ sender: Any) {
        self.menuClosingGlobal { (success) in }
    }
    
    @IBAction func buttonLogoutClicked(_ sender: CustomButton) {
        self.logout()
    }
    
    private func logout() {
        /*
        guard let delegate = SideMenuVC.delegate else {
            return
        }
        delegate.clickedLogout()
        */
        self.showAlertWithTitleOkCancelAction("Logout", message: "Do you really want to logout?", okTitle: "OK", cancelTitle: "Cancel", okAction: { [unowned self] in
            //UserDefaultsManager.shared.setStringValue(AppStrings.blankString, forKey: .acccessToken)
            /*
            UserDefaultsManager.shared.clearAllValuesExceptDeviceTokenAndLanguagePreference()
            RandomAPI.shared.stopRandomAPI()
            let initialNavigationController = InitialNavigationController.instantiateFromAppStoryboard(appStoryboard: .main)
            initialNavigationController.modalPresentationStyle = .fullScreen
            self.present(initialNavigationController, animated: true, completion: nil)
            */
           // self.callLogoutAPI()
        }) {
            
        }
    }
    
    func myProfileDetailAPICall() {
        let para: [String: Any] = ["customerId": userId, "userType": "customer"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.viewProfile, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                print(response)
                let modelData = MyProfileModel(fromDictionary: response)
                if let model = modelData.responseData {
               let firstName = model.firstName ?? ""
               let lastName = model.lastName ?? ""
               let email = model.email ?? ""
               let profileImage = model.profileImage ?? ""
                
                
                if profileImage.isValidImageURL() {
                    let profilePicture = profileImage
                    self.imageViewUserImage.sd_setImage(with: URL(string: profilePicture), placeholderImage: UIImage(named: "profile-icon-large"))
                } else {
                    self.imageViewUserImage.image = UIImage(named: "profile-icon-large")
                }
                self.labelUserName.text = "\(firstName) \(lastName)"
                self.labelUserPhone.text = "\(email)"
                }
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }

}

extension SideMenuVC: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.hud.show(in: self.view)
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hud.dismiss()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction("", message: message, okTitle: "OK", okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction("", message: message, okTitle: "OK", okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}

private typealias TableviewDelegateAndDatasource = SideMenuVC

extension TableviewDelegateAndDatasource : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuItemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableCell", for: indexPath) as! LeftMenuTableCell
        if self.menuArray[indexPath.row].isSubItem {
            cell.menuSubNameLabel.text = self.menuArray[indexPath.row].title//.localized()
            cell.menuSubNameLabel.isHidden = false
            cell.menuNameLbl.isHidden = true
            cell.menuImageView.isHidden = true
            cell.viewSeperator.isHidden = true
        } else {
            cell.menuNameLbl.text = self.menuArray[indexPath.row].title//.localized()
            cell.menuNameLbl.isHidden = false
            cell.menuSubNameLabel.isHidden = true
            /*
            if indexPath.row == self.menuItemsCount - 1 {
                //logout
                cell.menuImageView.isHidden = false
                cell.menuImageView.image = UIImage(named: "icon-logout")
                cell.viewSeperator.isHidden = false
            } else {
                cell.menuImageView.isHidden = true
                cell.viewSeperator.isHidden = true
            }
            */
            //-----
            cell.menuImageView.isHidden = false
            cell.menuImageView.image = UIImage(named: self.menuArray[indexPath.row].iconName ?? "")
            cell.viewSeperator.isHidden = true
            //-----
            if self.menuArray[indexPath.row].hasSubItems {
                cell.imageViewIconDropDown.isHidden = false
                if self.menuArray[indexPath.row].isExpanded {
                    cell.imageViewIconDropDown.image = UIImage(named: "icon-arrow-bottom-white")
                } else {
                    cell.imageViewIconDropDown.image = UIImage(named: "icon-arrow-right-white")
                }
            } else {
                cell.imageViewIconDropDown.isHidden = true
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.menuArray[indexPath.row].isSubItem {
            let subItem = self.menuArray[indexPath.row]
            if self.menuArray[subItem.menuItemIndexOfSubItem].isExpanded {
                return cellHeight
            } else {
                return 0
            }
        } else {
            return cellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.menuItemsCount - 1 {
            //Logout
            self.logout()
        } else {
            if menuArray[indexPath.row].hasSubItems {
                if menuArray[indexPath.row].isExpanded {
                    menuArray[indexPath.row].isExpanded = false
                } else {
                    menuArray[indexPath.row].isExpanded = true
                }
                tableView.reloadData()
            } else {
                self.menuClosingForPush(withIndexPath: indexPath)
            }
        }
        //-----
        /*
        if menuArray[indexPath.row].hasSubItems {
            if menuArray[indexPath.row].isExpanded {
                menuArray[indexPath.row].isExpanded = false
            } else {
                menuArray[indexPath.row].isExpanded = true
            }
            tableView.reloadData()
        } else {
            self.menuClosingForPush(withIndexPath: indexPath)
        }
        */
        //-----
    }
    
}

class LeftMenuTableCell: UITableViewCell {
    
    @IBOutlet weak var menuNameLbl: CustomNormalLabel!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuSubNameLabel: CustomNormalLabel!
    @IBOutlet weak var imageViewIconDropDown: UIImageView!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var menuView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
       // menuView.makeCircular()
        self.menuImageView.contentMode = .scaleAspectFit
        self.menuImageView.backgroundColor = .clear
        //self.menuImageView.isHidden = true
        self.menuSubNameLabel.textColor = .systemTeal
    }
    
}
