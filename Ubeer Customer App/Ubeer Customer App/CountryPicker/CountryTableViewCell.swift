//
//  CountryTableViewCell.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var labelCountryNameAndISDCode: UILabel!
    @IBOutlet weak var imageView_CountryFlag: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
