//
//  MyOrdersVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 15/04/22.
//

import UIKit

class MyOrdersVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnCurrentOrder: UIButton!
    @IBOutlet weak var btnPastOrder: UIButton!
    
    var orderStatus = "ONGOING"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getOrderListPICalling()
    }
    
    static func getInstance() -> MyOrdersVC {
        return R.storyboard.orderPlace.myOrdersVC()!
    }
    
    func updateButtonState(btnCurrentOrderColor: UIColor, btnCurrentOrderTextColor: UIColor, btnPastOrderColor: UIColor, btnPastOrderTextColor: UIColor) {
        btnCurrentOrder.backgroundColor = btnCurrentOrderColor
        btnCurrentOrder.setTitleColor(btnCurrentOrderTextColor, for: .normal)
        btnPastOrder.backgroundColor = btnPastOrderColor
        btnPastOrder.setTitleColor(btnPastOrderTextColor, for: .normal)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCurrentOrderAction(_ sender: Any) {
        orderStatus = "ONGOING"
        getOrderListPICalling()
        updateButtonState(btnCurrentOrderColor: R.color.orangeButtonColor()!, btnCurrentOrderTextColor: .white, btnPastOrderColor: R.color.buttonColorLightGrey()!, btnPastOrderTextColor: R.color.buttonTitleGrayColor()!)
    }
    
    @IBAction func btnPastOrderAction(_ sender: Any) {
        orderStatus = "PAST"
        getOrderListPICalling()
        updateButtonState(btnCurrentOrderColor: R.color.buttonColorLightGrey()!, btnCurrentOrderTextColor: R.color.buttonTitleGrayColor()!, btnPastOrderColor: R.color.orangeButtonColor()!, btnPastOrderTextColor: .white)
    }
}
extension MyOrdersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderTableViewCell", for: indexPath) as? MyOrderTableViewCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
}

//MARK: - API Calling
extension MyOrdersVC {
    func getOrderListPICalling() {
        let para: [String: Any] = ["customerId": "\(userId)", "orderStatus": orderStatus, "userType": "CUSTOMER"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.orderList, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                self.tblView.reloadData()
                print("s")
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}
