//
//  CheckoutModel.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 21/01/22.
//

import Foundation
import SwiftyJSON

// MARK: - CheckoutModel
class CheckoutModel {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: CheckoutResponseData?

    init(fromDictionary dictionary: JSON) {
        self.success = dictionary["success"].boolValue
        self.statuscode = dictionary["STATUSCODE"].intValue
        self.message = dictionary["message"].stringValue
        let response = dictionary["response_data"]
        self.responseData = CheckoutResponseData(fromDictionary: response)
    }
}

// MARK: - ResponseData
class CheckoutResponseData {
    var time, distance, deliveryCost, deliveryServiceCost: String?
    var foodServiceCost: String?

    init(fromDictionary dictionary: JSON) {
        self.time = dictionary["time"].stringValue
        self.distance = dictionary["distance"].stringValue
        self.deliveryCost = dictionary["deliveryCost"].stringValue
        self.deliveryServiceCost = dictionary["deliveryServiceCost"].stringValue
        self.foodServiceCost = dictionary["foodServiceCost"].stringValue
    }
}
