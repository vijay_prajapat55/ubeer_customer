//
//  AddressModel.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 11/04/22.
//

import Foundation
import SwiftyJSON

// MARK: - AddressModel
class AddressModel {
    var statuscode: Int?
    var responseData: AddressResponseData?
    var message: String?
    var success: Bool?

    init(fromDictionary dictionary: JSON) {
        self.success = dictionary["success"].boolValue
        self.statuscode = dictionary["STATUSCODE"].intValue
        self.message = dictionary["message"].stringValue
        let response = dictionary["response_data"]
        self.responseData = AddressResponseData(fromDictionary: response)
    }
}

// MARK: - ResponseData
class AddressResponseData {
    var address: [Address]?

    init(fromDictionary dictionary: JSON) {
        self.address = [Address]()
        let vendorArray = dictionary["address"].arrayValue
        
        for dic in vendorArray {
            let value = Address(fromDictionary: dic)
            self.address?.append(value)
        }
    }
}

// MARK: - Address
class Address {
    var customerID, longitude, houseNo, id: String?
    var createdAt, fullAddress: String?
    var v: Int?
    var updatedAt, addressType: String?
    var setDefault: Bool?
    var latitude: String?

    init(fromDictionary dictionary: JSON) {
        self.customerID = dictionary["customerId"].stringValue
        self.longitude = dictionary["longitude"].stringValue
        self.houseNo = dictionary["houseNo"].stringValue
        self.id = dictionary["_id"].stringValue
        self.createdAt = dictionary["createdAt"].stringValue
        self.fullAddress = dictionary["fullAddress"].stringValue
        self.v = dictionary["__v"].intValue
        self.updatedAt = dictionary["updatedAt"].stringValue
        self.addressType = dictionary["addressType"].stringValue
        self.setDefault = dictionary["setDefault"].boolValue
        self.latitude = dictionary["latitude"].stringValue
    }
}
