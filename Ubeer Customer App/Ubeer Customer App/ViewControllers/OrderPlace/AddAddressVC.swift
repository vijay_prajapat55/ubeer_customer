//
//  AddAddressVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 21/02/22.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class AddAddressVC: UIViewController {
    
    @IBOutlet weak var lblTitle: CustomNormalLabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtFldSearch: UITextField!
    @IBOutlet weak var txtFldHouseNumber: UITextField!
    @IBOutlet weak var txtFldDeliveryInstruction: UITextField!
    @IBOutlet weak var lblSaveAs: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var lblDefaultAddress: UILabel!
    
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    var locationManager = CLLocationManager()
    var lat = Double()
    var lng = Double()
    var currentLocation = ""
    var addressType = ""
    var isDefaultAddress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        self.mapView.settings.compassButton = true
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationManager.stopUpdatingLocation()
    }
    
    static func getInstance() -> AddAddressVC {
        return R.storyboard.orderPlace.addAddressVC()!
    }
    
    func isValidate() -> Bool {
        if addressType == "" {
            ErrorHanding().showAlert(message: "Please select address type.", vc: self)
            return false
        }
        return true
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        
    }
    
    @IBAction func btnHomeAction(_ sender: Any) {
        self.addressType = "Home"
    }
    
    @IBAction func btnWorkAction(_ sender: Any) {
        self.addressType = "Work"
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        self.addressType = "Other"
    }
    
    @IBAction func btnDefaultAddressAction(_ sender: Any) {
        self.isDefaultAddress = true
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.addAddressAPICalling()
    }
}

extension AddAddressVC: CLLocationManagerDelegate, GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        print("autocomplete")
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print("error")
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error" + error.description)
    }
}

//MARK: - Location Delegate
extension AddAddressVC {
    func checkAuthorisation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            break
        @unknown default:
            break
        }
    }
    
    func setupLocation() {
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler: {(placemarks, error) in
            if (error != nil) {
            }
            let pm = (placemarks ?? [CLPlacemark]()) as [CLPlacemark]
            
            if pm.count > 0 {
                let pm = placemarks![0]
                
                var addressString: String = ""
                if pm.subThoroughfare != nil {
                    addressString = addressString + pm.subThoroughfare! + ", "
                }
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode!
                }
                
                self.currentLocation = addressString
                print(addressString)
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.lat = latitude
        self.lng = longitude
        
        getAddressFromLatLon(pdblLatitude: self.lat, withLongitude: self.lng)
    }
}

extension AddAddressVC {
    func addAddressAPICalling() {
        if isValidate() {
            houseNumber = txtFldHouseNumber.text ?? ""
            deliveryLat = self.lat
            deliveryLong = self.lng
            deliveryAddressType = self.addressType
            
            let para: [String: Any] = ["customerId": userId, "userType": "CUSTOMER", "fullAddress": self.currentLocation, "houseNo": txtFldHouseNumber.text ?? "", "addressType": self.addressType, "latitude": self.lat, "longitude": self.lng, "setDefault": isDefaultAddress]
            
            APIService.postApiCallingWithHeaderMethod(param: para, url: API.addAddress, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    
                    print(response)
                default:
                    let message = response["message"].stringValue
                    ErrorHanding().showAlert(message: message, vc: self)
                }
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
}
