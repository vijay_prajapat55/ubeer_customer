//
//  SplashVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 29/12/21.
//

import UIKit
import SDWebImage

class SplashVC: UIViewController {

    @IBOutlet weak var gifImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGIF()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.navigationController?.pushViewController(TabbarVC.getInstance(), animated: true)
        }
    }
    
    static func getInstance() -> SplashVC {
        return R.storyboard.main.splashVC()!
    }
    
    func setupGIF() {
        let gif = UIImage.gifImageWithName("Beer-Gif")
        gifImgView.image = gif
    }
}

