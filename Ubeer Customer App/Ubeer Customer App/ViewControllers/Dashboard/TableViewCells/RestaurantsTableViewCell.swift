//
//  RestaurantsTableViewCell.swift
//
//  Created by Souvik on 23/03/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var image_Restaurant: UIImageView!
    @IBOutlet weak var label_Name: CustomNormalLabel!
    @IBOutlet weak var label_Description: CustomNormalLabel!
    @IBOutlet weak var label_rating: CustomNormalLabel!
    @IBOutlet weak var button_like: UIButton!
    @IBOutlet weak var labelDistanceOrPrice: CustomLightLabel!
    @IBOutlet weak var labelOffer: CustomNormalLabel!
    @IBOutlet weak var labelClosed: UILabel!
    @IBOutlet weak var labelTime: CustomNormalLabel!
    @IBOutlet weak var viewOverlayClosed: UIView!
    
    var likeVendor: ((_ cell: UITableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.label_Name.font = AppFont.getFont(style: .normal, size: .regular)
        self.label_Description.font = AppFont.getFont(style: .normal, size: .extraSmall)
        self.label_rating.font = AppFont.getFont(style: .normal, size: .extraSmall)
        self.labelDistanceOrPrice.font = AppFont.getFont(style: .normal, size: .small)
        self.labelOffer.font = AppFont.getFont(style: .normal, size: .small)
        self.labelTime.font = AppFont.getFont(style: .normal, size: .small)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.addCornerRadiusShadowBorder(
            cornerRadius: 5.0,
            borderWidth: 0.0,
            borderColor: .clear,
            shadowColor: .lightGray)
    }
    
    func setData(model: Vendor) {
        self.image_Restaurant.sd_setImage(with: URL(string : model.banner ?? ""), placeholderImage: UIImage(named: "DummyLogo")) { (img, err, type, url) in}
        self.label_Name.text = model.name
        var txtCategory = model.address
//        for cat in vendor.category ?? [] {
//            txtCategory += "\(cat.categoryName ?? ""), "
//        }
//        if txtCategory.count > 0 {
//            txtCategory = String(txtCategory.dropLast())
//        }
        self.label_Description.text = txtCategory
        self.label_rating.text = String(describing: 0.0)
//        if HomeScreen.deliveryType == .delivery {
//            self.labelDistanceOrPrice.text = "Delivery $\(vendor.deliveryCharge ?? 0.0)"
//            self.labelTime.isHidden = false
//        } else {
        self.labelDistanceOrPrice.text = model.distance
            self.labelTime.isHidden = true
     //   }
        
        self.labelTime.text = "  10 mins  "
        
//        if vendor.restaurantClose ?? false {
//            self.labelClosed.isHidden = false
//            self.viewOverlayClosed.isHidden = false
//        } else {
            self.labelClosed.isHidden = true
            self.viewOverlayClosed.isHidden = true
       // }
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE" // "EE" to get short style
        let dayInWeek = dateFormatter.string(from: date) // "Sunday
    //    if !(vendor.restaurantClose ?? false) {
//            for item in vendor.vendorTime ?? [] {
//                if (item.day ?? "") == dayInWeek {
//                    let formatter = DateFormatter()
//                    formatter.dateFormat = "hh:mm a"
//                    if item.openTime != "" {
//                        let startTime = formatter.date(from: item.openTime ?? "")
//                        let startHour = startTime?.get(.hour)
//                        let startMin = startTime?.get(.minute)
//
//                        if item.closeTime != "" {
//                            let endTime = formatter.date(from: item.closeTime ?? "")
//                            let endHour = endTime?.get(.hour)
//                            let endMin = endTime?.get(.minute)
//                            if Utility.checkTimeExist(startHour: Int(startHour ?? "0") ?? 0, startMin: Int(startMin ?? "0") ?? 0, endHour: Int(endHour ?? "0") ?? 0, endMin: Int(endMin ?? "0") ?? 0) {
//                                self.labelClosed.isHidden = true
//                                break
//                            } else {
//                                self.labelClosed.isHidden = false
//                            }
//                        } else {
//                            self.labelClosed.isHidden = false
//                        }
//                    } else {
//                        self.labelClosed.isHidden = false
//                    }
//                    break
//                } else {
//                    self.labelClosed.isHidden = false
//                }
//            }
       // } else {
            self.labelClosed.isHidden = true
       // }
        if model.favorite == 1 {
            self.button_like.setImage(UIImage(named: "like_button"), for: .normal)
        } else {
            self.button_like.setImage(UIImage(named: "heart"), for: .normal)
        }
        if model.offer == "" {
            self.labelOffer.isHidden = true
        } else {
            self.labelOffer.text = model.offer
        }
    }
    
    @IBAction func buttonLikeClicked(_ sender: Any) {
        self.likeVendor?(self)
    }
    
}
