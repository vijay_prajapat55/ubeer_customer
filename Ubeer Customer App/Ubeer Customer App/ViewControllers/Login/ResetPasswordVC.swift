//
//  ResetPasswordVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 04/01/22.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppFont.getFont(style: .normal, size: .large)
        }
    }
    @IBOutlet weak var lblPasswordNote: UILabel! {
        didSet {
            lblPasswordNote.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var txtFldPassword: UITextField! {
        didSet {
            txtFldPassword.font = AppFont.getFont(style: .normal, size: .regular)
        }
    }
    @IBOutlet weak var txtFldConfirmPassword: UITextField! {
        didSet {
            txtFldConfirmPassword.font = AppFont.getFont(style: .normal, size: .regular)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.layer.cornerRadius = UIScreen.main.bounds.height / 36
            btnSubmit.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> ResetPasswordVC {
        return R.storyboard.login.resetPasswordVC()!
    }
    
    func isValidate() -> Bool {
        if txtFldPassword.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterPassword, vc: self)
            return false
        }
        if txtFldConfirmPassword.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterConfirmPassword, vc: self)
            return false
        }
        return true
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        resetPasswordAPICall()
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEyePassword(_ sender: UIButton) {
        self.txtFldPassword.isSecureTextEntry = !self.txtFldPassword.isSecureTextEntry
        sender.setImage(self.txtFldPassword.isSecureTextEntry ? UIImage(named: "eyeClosed") : UIImage(named: "eyeOpen"), for: .normal)
    }
    
    @IBAction func btnEyeConfirmPassword(_ sender: UIButton) {
        self.txtFldConfirmPassword.isSecureTextEntry = !self.txtFldConfirmPassword.isSecureTextEntry
        sender.setImage(self.txtFldConfirmPassword.isSecureTextEntry ? UIImage(named: "eyeClosed") : UIImage(named: "eyeOpen"), for: .normal)
    }
}

//MARK: - API Calling
extension ResetPasswordVC {
    func resetPasswordAPICall() {
        if isValidate() {
            let para: [String: Any] = ["phone": phoneNumber, "userType": "customer", "password": txtFldPassword.text!, "confirmPassword": txtFldConfirmPassword.text!, "deviceToken": pushDeviceToken, "appType": "IOS", "pushMode": "P"]
            
            APIService.postApiCallingWithHeaderMethod(param: para, url: API.resetPassword, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    print(response)
                    let vc = LoginVC.getInstance()
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    let message = response["message"].stringValue
                    ErrorHanding().showAlert(message: message, vc: self)
                }
                
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
}
