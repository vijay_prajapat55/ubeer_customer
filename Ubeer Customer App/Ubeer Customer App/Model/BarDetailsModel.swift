//
//  BarDetailsModel.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 13/01/22.
//

import Foundation
import SwiftyJSON

// MARK: - BarDetailsModel
class BarDetailsModel {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: BarResponseData?

    init(fromDictionary dictionary: JSON) {
        self.success = dictionary["success"].boolValue
        self.statuscode = dictionary["STATUSCODE"].intValue
        self.message = dictionary["message"].stringValue
        let response = dictionary["response_data"]
        self.responseData = BarResponseData(fromDictionary: response)
    }
}

// MARK: - ResponseData
class BarResponseData {
    var restaurant: Restaurant?
    var vendorTime: VendorTime?
    var catItem: Catitem?

    init(fromDictionary dictionary: JSON) {
         let restaurant = dictionary["restaurant"]
        self.restaurant = Restaurant(fromDictionary: restaurant)
        
        let vendorTime = dictionary["vendorTime"]
       self.vendorTime = VendorTime(fromDictionary: vendorTime)
        
        let catitem = dictionary["catitem"]
       self.catItem = Catitem(fromDictionary: catitem)
    }
}

// MARK: - Catitem
class Catitem {
    var item: [Item]
    var category: BarCategory?

    init(fromDictionary dictionary: JSON) {
        self.item = [Item]()
        let itemArray = dictionary["item"].arrayValue
        
        for dic in itemArray {
            let value = Item(fromDictionary: dic)
            self.item.append(value)
        }
        
        let category = dictionary["category"]
       self.category = BarCategory(fromDictionary: category)
    }
}

// MARK: - Category
class BarCategory {
    var data: [Datum]
    var imageURL: String?

    init(fromDictionary dictionary: JSON) {
        self.data = [Datum]()
        let dataArray = dictionary["data"].arrayValue
        
        for dic in dataArray {
            let value = Datum(fromDictionary: dic)
            self.data.append(value)
        }
        
        self.imageURL = dictionary["imageUrl"].stringValue
    }
}

// MARK: - Datum
class Datum {
    var categoryName, image, id: String?

    init(fromDictionary dictionary: JSON) {
        self.categoryName = dictionary["categoryName"].stringValue
        self.image = dictionary["image"].stringValue
        self.id = dictionary["_id"].stringValue
    }
}

// MARK: - Item
class Item {
    var itemID, categoryID, itemName: String?
    var price: Double?
    var itemDescription: String?
    var menuImage: String?

    init(fromDictionary dictionary: JSON) {
        self.itemID = dictionary["itemId"].stringValue
        self.categoryID = dictionary["categoryId"].stringValue
        self.itemName = dictionary["itemName"].stringValue
        self.price = dictionary["price"].doubleValue
        self.itemDescription = dictionary["description"].stringValue
        self.menuImage = dictionary["menuImage"].stringValue
    }
}

// MARK: - Restaurant
class Restaurant {
    var name: String?
    var rating: Int?
    var logo: String?
    var banner: String?
    var restaurantClose: Bool?
    var address, id: String?
    var latitude, longitude: Double?
    var distance: String?
    var deliveryCharge: Int?

    init(fromDictionary dictionary: JSON) {
        self.name = dictionary["name"].stringValue
        self.rating = dictionary["rating"].intValue
        self.logo = dictionary["logo"].stringValue
        self.banner = dictionary["banner"].stringValue
        self.restaurantClose = dictionary["restaurantClose"].boolValue
        self.address = dictionary["address"].stringValue
        self.id = dictionary["id"].stringValue
        self.latitude = dictionary["latitude"].doubleValue
        self.longitude = dictionary["longitude"].doubleValue
        self.distance = dictionary["distance"].stringValue
        self.deliveryCharge = dictionary["deliveryCharge"].intValue
    }
}

// MARK: - VendorTime
class VendorTime {
    var openTime, closeTime: String?

    init(fromDictionary dictionary: JSON) {
        self.openTime = dictionary["openTime"].stringValue
        self.closeTime = dictionary["closeTime"].stringValue
    }
}
