//
//  MyProfileVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 07/01/22.
//

import UIKit

class MyProfileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> MyProfileVC {
        return R.storyboard.dashboard.myProfileVC()!
    }

}
