//
//  LoginVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 04/01/22.
//

import UIKit
import SwiftyJSON
import Toast_Swift

class LoginVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppFont.getFont(style: .bold, size: .extraLarge)
        }
    }
    @IBOutlet weak var lblEmail: UILabel! {
        didSet {
            lblEmail.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldEmail: UITextField! {
        didSet {
            txtFldEmail.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    
    @IBOutlet weak var lblPassword: UILabel! {
        didSet {
            lblPassword.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var txtFldPassword: UITextField! {
        didSet {
            txtFldPassword.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var btnSignUp: UIButton! {
        didSet {
            btnSignUp.titleLabel?.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var lblAlreadyHaveAnAccount: UILabel! {
        didSet {
            lblAlreadyHaveAnAccount.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var btnForgotPassword: UIButton! {
        didSet {
            btnForgotPassword.titleLabel?.font = AppFont.getFont(style: .normal, size: .verySmall)
        }
    }
    @IBOutlet weak var btnSignIn: UIButton! {
        didSet {
            btnSignIn.titleLabel?.font = AppFont.getFont(style: .normal, size: .small)
            btnSignIn.layer.cornerRadius = UIScreen.main.bounds.height / 36
        }
    }
    
    var isComeFromSignUp = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> LoginVC {
        return R.storyboard.login.loginVC()!
    }
    
    func isValidate() -> Bool {
        if txtFldEmail.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterEmail, vc: self)
            return false
        }
        if txtFldPassword.text! == "" {
            ErrorHanding().showAlert(message: AppStrings.enterPassword, vc: self)
            return false
        }
        return true
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        self.navigationController?.pushViewController(ForgotPasswordVC.getInstance(), animated: true)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        if isComeFromSignUp {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.pushViewController(CreateAccountVC.getInstance(), animated: true)
        }
    }
    
    @IBAction func btnEyePasswordAction(_ sender: UIButton) {
        self.txtFldPassword.isSecureTextEntry = !self.txtFldPassword.isSecureTextEntry
        sender.setImage(self.txtFldPassword.isSecureTextEntry ? UIImage(named: "eyeClosed") : UIImage(named: "eyeOpen"), for: .normal)
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
        loginAPICall()
    }
}

//MARK: - API Calling
extension LoginVC {
    func loginAPICall() {
        if isValidate() {
            let para: [String: Any] = ["user": txtFldEmail.text!, "password": txtFldPassword.text!, "userType": "customer", "loginType": "EMAIL", "deviceToken": pushDeviceToken, "appType": "IOS", "pushMode": "P"]
            
            APIService.postApiCallingMethod(param: para, url: API.login, vc: self, showProgress: true) { response, isSuccess, statusCode in
                switch statusCode {
                case 200, 201, 204:
                    let data = response["response_data"]
                    let success = response["success"].boolValue
                    let message = response["message"].stringValue
                    self.view.makeToast(message)
                    if success {
                        authorisationToekn = data["authToken"].stringValue
                        let userDetails = data["userDetails"]
                        customerName = "\(userDetails["firstName"].stringValue) \(userDetails["lastName"].stringValue)"
                        userId = userDetails["id"].stringValue
                        userPhone = userDetails["phone"].stringValue
                        customerEmailID = userDetails["email"].stringValue
                        let vc = TabbarVC.getInstance()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                default:
                    let message = response["message"].stringValue
                    self.view.makeToast(message)
                }
                
            } failure: { error in
                print(error.localizedDescription)
            } noInternet: {
                print("no internet")
                return
            }
        }
    }
}

