//
//  DashboardVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 05/01/22.
//

import UIKit
import CoreLocation
import SwiftyJSON

class DashboardVC: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tableViewRestaurants: UITableView!
    @IBOutlet weak var labelAddress: CustomNormalLabel!
    @IBOutlet weak var viewCartButton: UIView!
    @IBOutlet weak var labelItemCount: UILabel!
    @IBOutlet weak var notificationBell: UIButton!
    
    //MARK: - Variables
    private var lat = CLLocationDegrees()
    private var lng = CLLocationDegrees()
    private var x = 1
    private var timer = Timer()
    private var ifBannerSelected = false
    private var locationManager = CLLocationManager()
    private var model: DashboardModel?
    private var address = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewRestaurants.register(UINib(nibName: "DashboardOptionsContainerTableViewCell", bundle: .main), forCellReuseIdentifier: "DashboardOptionsContainerTableViewCell")
        self.tableViewRestaurants.register(UINib(nibName: "DashboardCategoryContainerTableViewCell", bundle: .main), forCellReuseIdentifier: "DashboardCategoryContainerTableViewCell")
        self.tableViewRestaurants.register(UINib(nibName: "DashboardBannersContainerTableViewCell", bundle: .main), forCellReuseIdentifier: "DashboardBannersContainerTableViewCell")
        self.tableViewRestaurants.register(UINib(nibName: "RestaurantsTableViewCell", bundle: nil), forCellReuseIdentifier: "restaurantsTableViewCell")
        self.tableViewRestaurants.dataSource = self
        self.tableViewRestaurants.delegate = self
    }
    
    static func getInstance() -> DashboardVC {
        return R.storyboard.dashboard.dashboardVC()!
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getLatLong()
        self.labelAddress.text = userAddress
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer.invalidate()
    }
 
    private func getLatLong() {
        var currentLoc = CLLocation()
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            currentLoc = locationManager.location ?? CLLocation()
            self.lat = 23.5026205
            self.lng = 86.6812333
            userLat = self.lat
            userLong = self.lng
            print(currentLoc.coordinate.latitude)
            print(currentLoc.coordinate.longitude)
            self.dashboardAPICalling()
            self.getAddressAPICalling()
        case .authorizedWhenInUse:
            currentLoc = locationManager.location ?? CLLocation()
            self.lat = 23.5026205
            //currentLoc.coordinate.latitude
            self.lng = 86.6812333
            //currentLoc.coordinate.longitude
            userLat = self.lat
            userLong = self.lng
            print(currentLoc.coordinate.latitude)
            print(currentLoc.coordinate.longitude)
            self.dashboardAPICalling()
            self.getAddressAPICalling()
        default:
            return
        }
    }
    
    func getTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [.hour, .minute]
        let components = calendar.dateComponents(requestedComponents, from: date)
        let hour = components.hour!
        let minutes = components.minute!
        return "\(hour):\(minutes)"
    }
    
    @IBAction func btnSideMenuAction(_ sender: Any) {
        SideMenuVC.showLeftMenu(onParentViewController: self) { (selectedObj, selectedPosition) in
            
        }
        SideMenuVC.setDelegate(self)
//        let menuVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
//        self.navigationController?.pushViewController(menuVC, animated: true)
    }
}

extension DashboardVC: UITableViewDataSource, UITableViewDelegate {
    
    //0 section - Categories
    //1 section - Banner
    //2 section - Restaurant List
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1
        } else if section == 2 {
            return self.model?.responseData?.vendor.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
            headerView.backgroundColor = .white
            let headerLabel = UILabel(frame: CGRect(x: 8, y: 8, width: tableView.bounds.width, height: 24))
            headerLabel.font = AppFont.getFont(style: .normal, size: .regular)
            headerLabel.text = "Tavern - Pub near you"
            headerLabel.textColor = UIColor(named: "TextColorLight")
            headerView.addSubview(headerLabel)
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            //Restaurant List
            return tableView.bounds.width * (25 / 35) //width : height = 25 : 39
        case 1:
            //Banner
            return tableView.bounds.width * (115 / 320)
        case 0:
            //Categories
            return tableView.bounds.width * (110 / 320)
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 2:
            //Restaurant List
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantsTableViewCell") as? RestaurantsTableViewCell else {
                return UITableViewCell()
            }
            cell.setData(model: (self.model?.responseData?.vendor[indexPath.row])!)
            return cell
        case 1:
            //Banner
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardBannersContainerTableViewCell", for: indexPath) as? DashboardBannersContainerTableViewCell else {
                return UITableViewCell()
            }
            cell.model = self.model?.responseData?.bannerData
            cell.baseUrl = self.model?.responseData?.bannerImageURL ?? ""
            cell.reload(model: self.model?.responseData?.bannerData ?? [])
            cell.collectionViewBanner.reloadData()
            return cell
        case 0:
            //Categories
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCategoryContainerTableViewCell", for: indexPath) as? DashboardCategoryContainerTableViewCell else {
                return UITableViewCell()
            }
            cell.model = self.model?.responseData?.categoryData
            cell.baseUrl = self.model?.responseData?.categoryImageURL ?? ""
            cell.collectionViewCategory.reloadData()
            return cell
        default:
            print("no cell")
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = BarDetailsVC.getInstance()
        vc.lat = self.lat
        vc.lng = self.lng
        vc.address = self.address
        vc.vendorId = self.model?.responseData?.vendor[indexPath.row].id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - API Calling
extension DashboardVC {
    func dashboardAPICalling() {
        let time = self.getTime()
        let para: [String: Any] = ["customerId": userId, "categoryId": "1", "latitude": self.lat, "longitude": self.lng, "userType": "CUSTOMER", "sort": "DISTANCE", "currentDay": Date().dayOfWeek()!, "currentTime": time]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.dashboard, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                self.model = DashboardModel(fromDictionary: response)
                self.tableViewRestaurants.reloadData()
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
    
    func getAddressAPICalling() {
        if isDefaultAddressSaved {
            return
        }
        let para: [String: Any] = ["customerId": userId, "latitude": self.lat, "longitude": self.lng, "userType": "CUSTOMER"]
        
        APIService.postApiCallingWithHeaderMethod(param: para, url: API.physicalAddressByLatlong, vc: self, showProgress: true) { response, isSuccess, statusCode in
            switch statusCode {
            case 200, 201, 204:
                let responseData = response["response_data"].dictionaryValue
                if let address = responseData["address"]?.dictionaryValue {
                    let results = address["results"]?.arrayValue
                    let data = results?.first
                    userAddress = data?["formatted_address"].stringValue ?? ""
                    deliveryLat = self.lat
                    deliveryLong = self.lng
                    isDefaultAddressSaved = true
                    self.labelAddress.text = userAddress
                    self.address = userAddress
                }
            default:
                let message = response["message"].stringValue
                ErrorHanding().showAlert(message: message, vc: self)
            }
        } failure: { error in
            print(error.localizedDescription)
        } noInternet: {
            print("no internet")
            return
        }
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}

extension DashboardVC: SideMenuVCDelegate {
    
    func clickedMenuHeader() {
        
    }
    
    func clickedIndexPath(index: Int) {
        switch index {
        case 0:
            break
        case 1:
            print("")
//            let profileTVC = ProfileTableViewController.instantiateFromAppStoryboard(appStoryboard: .profile)
//            self.navigationController?.pushViewController(profileTVC, animated: true)
        case 2:
            print("")
            self.navigationController?.pushViewController(MyOrdersVC.getInstance(), animated: true)
//            let vc = NotificationSettingsView.instantiateFromAppStoryboard(appStoryboard: .notification)
//            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            print("")
//            let vc = EarningViewController.instantiateFromAppStoryboard(appStoryboard: .home)
//            self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            print("")
//            let vc = OrderListViewController.instantiateFromAppStoryboard(appStoryboard: .home)
//            self.navigationController?.pushViewController(vc, animated: true)
        case 5:
            print("")
            //self.logout()
            
        default:
            break
        }
    }
    
    private func logout() {
        self.showAlertWithTitleOkCancelAction("Logout", message: "Do you really want to logout?", okTitle: "OK", cancelTitle: "Cancel", okAction: { [unowned self] in
           // self.callLogoutAPI()
        }) {
            
        }
    }
    
    func clickedLogout() {
        
    }
    
}
