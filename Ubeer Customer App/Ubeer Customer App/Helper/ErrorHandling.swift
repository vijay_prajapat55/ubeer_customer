//
//  ErrorHandling.swift
//  DietPro
//

import Foundation
import SwiftyJSON
import UIKit

class ErrorHanding: NSObject {
    
    func isSuccess(_ statusCode: Int?) -> Bool {
        switch statusCode {
        case 200, 201, 204:
            return true
        default:
            return false
        }
    }
    
    func getError(response: JSON) -> String {
        for cnt in 0..<response.count {
            if let message = response[cnt]["message"].string, !message.isEmpty {
                return message
            }
        }
        if let message = response["message"].string, !message.isEmpty {
            return message
        }
        return "There is an error."
    }
    
    func showAlert(message: String, vc: UIViewController) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
}
