//
//  DashBoardOffersCollectionViewCell.swift
//
//  Created by Souvik on 23/03/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class DashBoardOffersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image_Offier: UIImageView!
    @IBOutlet weak var label_Offers: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
