//
//  AddCardVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 12/04/22.
//

import UIKit

class AddCardVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCardDetails: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var txtFldCardNumber: UITextField!
    @IBOutlet weak var lblValidUntil: UILabel!
    @IBOutlet weak var txtFldValidUntil: UITextField!
    @IBOutlet weak var lblCVV: UILabel!
    @IBOutlet weak var txtFldCVV: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    static func getInstance() -> AddCardVC {
        return R.storyboard.orderPlace.addCardVC()!
    }
    
    @IBAction func btnNotificationAction(_ sender: Any) {
        
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayAction(_ sender: Any) {
        
    }
}
