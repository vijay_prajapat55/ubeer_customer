//
//  RestaurantMenuItemsDisplayTableViewCell.swift
//  Customer
//
//  Created by Pradipta on 20/08/21.
//  Copyright © 2021 Bit Mini. All rights reserved.
//

import UIKit

class RestaurantMenuItemsDisplayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var labelRestaurantName: CustomNormalLabel! {
        didSet {
            labelRestaurantName.font = AppFont.getFont(style: .normal, size: .extraSmall)
        }
    }
    @IBOutlet weak var labelItemDescription: CustomNormalLabel! {
        didSet {
            labelItemDescription.font = AppFont.getFont(style: .normal, size: .tabbarSize)
        }
    }
    @IBOutlet weak var buttonItemDescriptionReadMore: UIButton!
    @IBOutlet weak var labelItemPrice: CustomNormalLabel! {
        didSet {
            labelItemPrice.font = AppFont.getFont(style: .normal, size: .small)
        }
    }
    
    var itemDescriptionReadMoreClicked: ((UITableViewCell) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.addCornerRadiusShadowBorder(
            cornerRadius: 5.0,
            borderWidth: 0.0,
            borderColor: .clear,
            shadowColor: .lightGray)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewBackground.addCornerRadiusShadowBorder(
            cornerRadius: 5.0,
            borderWidth: 0.0,
            borderColor: .clear,
            shadowColor: .lightGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func buttonItemDescriptionReadMoreClicked(_ sender: Any) {
        self.itemDescriptionReadMoreClicked?(self)
    }
    
    func setItemData(model: Item) {
        self.imageViewMenuItem.sd_setImage(with: URL(string : model.menuImage ?? ""), placeholderImage: UIImage(named: "DummyLogo")) { (img, err, type, url) in}
        self.labelRestaurantName.text = model.itemName
        self.labelItemPrice.text = "\(model.price ?? 0)"
        
        //let timeToDisplay = calculateTimeInDetails(min: foodItem?.waitingTime ?? 0)
        //let categoryArray = self.presenter.objRestaurantDetails?.responseData?.catitem?.category?.data?.filter { $0.id == foodItem?.categoryID }
        //let categoryNamesArray = categoryArray?.compactMap { $0.categoryName }
        //cell.labelItemDescription.text = "\(foodItem?.type ?? ""), \(categoryNamesArray?.joined(separator: ", ") ?? "")\n\(timeToDisplay)"
        
        self.labelItemDescription.text = model.itemDescription
   //     self.buttonItemDescriptionReadMore.isHidden = (foodItem.itemDescription?.trimmingCharacters(in: .whitespacesAndNewlines) == "")
    }
    
}
