//
//  EnterManuallyTipVC.swift
//  Ubeer Customer App
//
//  Created by Vijay Prajapat on 24/01/22.
//

import UIKit

protocol ManualTipProtocol {
    func getManualTip(tipAmount: String)
}

class EnterManuallyTipVC: UIViewController {

    @IBOutlet weak var txtFldTipAmount: UITextField!
    @IBOutlet weak var btnOk: UIButton!
    
    var delegate: ManualTipProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> EnterManuallyTipVC {
        return R.storyboard.orderPlace.enterManuallyTipVC()!
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOkAction(_ sender: UIButton) {
        delegate?.getManualTip(tipAmount: txtFldTipAmount.text!)
        self.dismiss(animated: true, completion: nil)
    }

}
